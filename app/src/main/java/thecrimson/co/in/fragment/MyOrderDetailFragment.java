package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.MyOrderDetailAdapter;
import thecrimson.co.in.adapter.OrderDetailGstAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.OrderDetailDataPOzo;
import thecrimson.co.in.pozo.OrderDetailPozo;
import thecrimson.co.in.pozo.OrderListDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;

public class MyOrderDetailFragment extends Fragment {
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    RecyclerView recyclerview;
    ProgressBar progressbar;
    TextView orderno;
    TextView orderdate;
    OrderListDataPozo orderlsitdata;
    TextView retailprice;
    TextView discount;
    TextView saleprice;
    RecyclerView gstrecycleview;
    TextView grandtotal;
    LinearLayout belowlayout;
    RelativeLayout saving_layout;

    public MyOrderDetailFragment() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setHasOptionsMenu(true);
        orderlsitdata = (OrderListDataPozo) getArguments().getSerializable("orderdata");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_order_detail, container, false);
        initview(view);
        bindevent();
        if (!NetworkUtil.isNetworkConnected(context)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }
        clickListener();
        return view;
    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);
        Data data = new Data();
        data.setOrderid(Integer.parseInt(orderlsitdata.getOrderId()));


        final Gson gson = new Gson();
        final String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"orderdetial")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            belowlayout.setVisibility(View.VISIBLE);

                            Gson gson = new Gson();

                            OrderDetailPozo datum = gson.fromJson(response.toString(), OrderDetailPozo.class);
                            OrderDetailDataPOzo oredrdata = datum.getData();

                            if(oredrdata.getSaving().equals("0")){

                                saving_layout.setVisibility(View.GONE);
                            }else {
                                saving_layout.setVisibility(View.VISIBLE);
                            }

                            retailprice.setText("Rs "+oredrdata.getTotalPrice());
                            saleprice.setText("Rs "+oredrdata.getSubTotal());
                            discount.setText("Rs "+oredrdata.getSaving());

                            OrderDetailGstAdapter adapter = new OrderDetailGstAdapter(oredrdata.getGst());
                            gstrecycleview.setAdapter(adapter);

                            grandtotal.setText("Rs "+oredrdata.getGrandTotal());
                            MyOrderDetailAdapter adapter1 = new MyOrderDetailAdapter(oredrdata.getItem(),context);
                            recyclerview.setAdapter(adapter1);
                        }else if(code!=null && code.equals("500")){
                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new MyOrderListFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }

    private void bindevent() {
        txtToolbar.setText("ORDER DETAIL");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);


        orderno.setText("Order No: "+orderlsitdata.getOrderNumber());
        orderdate.setText("Order Date: "+orderlsitdata.getOrderDate());

    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recyclerview=view.findViewById(R.id.recyclerview);
        progressbar=view.findViewById(R.id.progressbar);
        orderdate=view.findViewById(R.id.orderdate);
        orderno=view.findViewById(R.id.orderno);
        retailprice=view.findViewById(R.id.retailprice);
        discount=view.findViewById(R.id.discount);
        saleprice=view.findViewById(R.id.saleprice);
        belowlayout=view.findViewById(R.id.belowlayout);
        gstrecycleview=view.findViewById(R.id.gstrecycleview);
        saving_layout=view.findViewById(R.id.saving_layout);
        gstrecycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        grandtotal=view.findViewById(R.id.grandtotal);
        recyclerview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;
    }

}
