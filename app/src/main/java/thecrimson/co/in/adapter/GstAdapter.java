package thecrimson.co.in.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.GstPozo;

public class GstAdapter extends RecyclerView.Adapter<GstAdapter.MyViewHolder>{
    List<GstPozo> gstlist;

    public GstAdapter(List<GstPozo> gstlist) {

        this.gstlist=gstlist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_gst, parent, false);

        return new GstAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if(gstlist.get(position).getAmount()!=0) {
            holder.gstpercentage.setText("GST @ " + gstlist.get(position).getPercentage());
            holder.gstamount.setText("+ Rs " + String.valueOf(new DecimalFormat("##.##").format(gstlist.get(position).getAmount())));
        }
    }

    @Override
    public int getItemCount() {
        return gstlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView gstpercentage;
        TextView gstamount;
        public MyViewHolder(View itemView) {
            super(itemView);
            gstamount=itemView.findViewById(R.id.gstamount);
            gstpercentage=itemView.findViewById(R.id.gstpercentage);
        }
    }
}
