package thecrimson.co.in.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.BrandAdapter;
import thecrimson.co.in.adapter.NewArrivalAdapter;
import thecrimson.co.in.adapter.ProductAdapter;
import thecrimson.co.in.adapter.SliderPagerAdapter;
import thecrimson.co.in.adapter.TrendingAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.BrandDataPozo;
import thecrimson.co.in.pozo.BrandPozo;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.CategoryPozo;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.Data2;
import thecrimson.co.in.pozo.Data3;
import thecrimson.co.in.pozo.PromotionalBannerDataPozo;
import thecrimson.co.in.pozo.PromotionalBannerPozo;
import thecrimson.co.in.pozo.TrendingDataPozo;
import thecrimson.co.in.pozo.TrendingPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class HomeFragment extends Fragment {
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    private ViewPager vp_slider;
    private LinearLayout ll_dots;
    SliderPagerAdapter sliderPagerAdapter;
    private TextView[] dots;
    private int NUM_PAGES;
    private static int currentPage = 0;
    RecyclerView recycleview;
    TextView viewall;
    TextView viewallnewarrival;
    TextView viewalltoptrending;
    RecyclerView brandrecycleview;
    RecyclerView trendingrecycleview;
    RecyclerView newarrivalrecycleview;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    ProgressBar progressbar;
    private ArrayList<BrandDataPozo> branddata;
    RelativeLayout brandlayout;
    private ArrayList<TrendingDataPozo> productdata;
    private ArrayList<TrendingDataPozo> productdata1;
    RelativeLayout ll_newarival;
    RelativeLayout ll_toptrending;
    LinearLayout llviewall_toptrending;
    LinearLayout llviewall_newarival;
    private List<PromotionalBannerDataPozo> banerdata;
    View line;
    private Timer swipeTimer;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    @Override
    public void onStop() {
        super.onStop();
       swipeTimer.cancel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        initview(view);
        bindevent();
        clickListener();

        if (!NetworkUtil.isNetworkConnected(context)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCallingPromotionalbanner();
            ApiCallingCategory();
            ApiCallingBrand();
            ApiCallingNewArrival();
            ApiCallingTopTrending();
        }




            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    vp_slider.setCurrentItem(currentPage++, true);
                }
            };
            swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2000, 2000);


        return view;
    }

    private void ApiCallingPromotionalbanner() {

        progressbar.setVisibility(View.VISIBLE);

        AndroidNetworking.post(Constant.Url+"promotion-banner")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Gson gson = new Gson();
                            PromotionalBannerPozo datum = gson.fromJson(response.toString(), PromotionalBannerPozo.class);
                            banerdata = datum.getData();

                            sliderPagerAdapter = new SliderPagerAdapter(context, banerdata);
                            vp_slider.setAdapter(sliderPagerAdapter);

                            addBottomDots(0);

                            NUM_PAGES =banerdata.size();



                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void ApiCallingTopTrending() {

        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Data3 data = new Data3();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setTop_trend(1);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        ll_toptrending.setVisibility(View.VISIBLE);
                        progressbar.setVisibility(View.GONE);
                        brandlayout.setVisibility(View.VISIBLE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                             productdata1 = datum.getData();

                             if(productdata1.size()<=2){

                                 llviewall_toptrending.setVisibility(View.GONE);
                             }else {
                                 llviewall_toptrending.setVisibility(View.VISIBLE);
                             }

                            TrendingAdapter trendingadapter =new TrendingAdapter(context,productdata1);
                            trendingrecycleview.setAdapter(trendingadapter);

                        }else if(code!=null && code.equals("500")){
                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void ApiCallingNewArrival() {

        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Data2 data = new Data2();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setNew_arrival(1);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        ll_newarival.setVisibility(View.VISIBLE);
                        brandlayout.setVisibility(View.VISIBLE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                            productdata = datum.getData();

                            if(productdata.size()<=2){

                                llviewall_newarival.setVisibility(View.GONE);
                            }else {
                                llviewall_newarival.setVisibility(View.VISIBLE);
                            }
                            NewArrivalAdapter newarrivaladapter = new NewArrivalAdapter(context,productdata);
                            newarrivalrecycleview.setAdapter(newarrivaladapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void ApiCallingBrand() {

        AndroidNetworking.post(Constant.Url+"brand")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        brandlayout.setVisibility(View.VISIBLE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            Gson gson = new Gson();
                            BrandPozo datum = gson.fromJson(response.toString(), BrandPozo.class);

                            List<String> list = new ArrayList<>();
                            list.add("djsd");
                            branddata = datum.getData();
                            BrandAdapter brandadapter = new BrandAdapter(branddata,context);
                            brandrecycleview.setAdapter(brandadapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void ApiCallingCategory() {


        Data data = new Data();
        data.setParent_id(0);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"category")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        line.setVisibility(View.VISIBLE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            CategoryPozo datum = gson.fromJson(response.toString(), CategoryPozo.class);

                            List<CategoryDataPozo> categorydata = datum.getData();

                            ProductAdapter adapter = new ProductAdapter(context,categorydata);
                            recycleview.setAdapter(adapter);
                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                ((MainActivity)getActivity()).openDrawer();
            }
        });

        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                addBottomDots(position);

            }

            @Override
            public void onPageSelected(int position) {
                currentPage=position;
                addBottomDots(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new BrandFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("branddata",branddata);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        viewalltoptrending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new ViewAllFragment();
                Bundle bundle = new Bundle();
                bundle.putString("viewalltag","toptrending");
                bundle.putSerializable("productdata",productdata1);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


        viewallnewarrival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new ViewAllFragment();
                Bundle bundle = new Bundle();
                bundle.putString("viewalltag","newarrival");
                bundle.putSerializable("productdata",productdata);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    private void addBottomDots(int position) {

        dots = new TextView[banerdata.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(25);
            dots[i].setTextColor(Color.parseColor("#FFFFFF"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[position].setTextColor(Color.parseColor("#ffe400"));
    }

    private void bindevent() {
        txtToolbar.setText("HOME");

        toolbar.setNavigationIcon(R.drawable.menu);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private void initview(View view) {


        NUM_PAGES=-1;
        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        vp_slider =  view.findViewById(R.id.vp_slider);
        ll_dots = view.findViewById(R.id.ll_dots);
        brandlayout = view.findViewById(R.id.brandlayout);
        recycleview = view.findViewById(R.id.recycleview);
        progressbar = view.findViewById(R.id.progressbar);
        viewall = view.findViewById(R.id.viewall);
        viewallnewarrival = view.findViewById(R.id.viewallnewarrival);
        viewalltoptrending = view.findViewById(R.id.viewalltoptrending);
        brandrecycleview = view.findViewById(R.id.brandrecycleview);
        trendingrecycleview = view.findViewById(R.id.trendingrecycleview);
        newarrivalrecycleview = view.findViewById(R.id.newarrivalrecycleview);
        ll_toptrending = view.findViewById(R.id.ll_toptrending);
        ll_newarival = view.findViewById(R.id.ll_newarival);
        llviewall_newarival = view.findViewById(R.id.llviewall_newarival);
        llviewall_toptrending = view.findViewById(R.id.llviewall_toptrending);
        line = view.findViewById(R.id.view);
        recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        brandrecycleview.setLayoutManager(new GridLayoutManager(context,3));
        trendingrecycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        newarrivalrecycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        recycleview.setNestedScrollingEnabled(false);
        brandrecycleview.setNestedScrollingEnabled(false);
        trendingrecycleview.setNestedScrollingEnabled(false);
        newarrivalrecycleview.setNestedScrollingEnabled(false);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;

    }


}
