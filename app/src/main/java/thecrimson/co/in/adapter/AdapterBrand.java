package thecrimson.co.in.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ViewAllFragment;
import thecrimson.co.in.pozo.BrandDataPozo;

public class AdapterBrand extends RecyclerView.Adapter<AdapterBrand.MyViewHolder>{
    List<BrandDataPozo> branddata;
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    public AdapterBrand(List<BrandDataPozo> branddata, MainActivity context) {

        this.branddata=branddata;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_brand, parent, false);

        return new AdapterBrand.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.brandname.setText(branddata.get(position).getName());

        Picasso.get().load(branddata.get(position).getImage()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                holder.brandimage.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new ViewAllFragment();
                Bundle bundle = new Bundle();
                bundle.putString("viewalltag","brand");
                bundle.putInt("brandid",branddata.get(position).getId());
                bundle.putString("brandname",branddata.get(position).getName());
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return branddata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView brandimage;
        LinearLayout backgroundlayout;
        TextView brandname;
        public MyViewHolder(View itemView) {
            super(itemView);

            brandimage=itemView.findViewById(R.id.brandimage);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
            brandname=itemView.findViewById(R.id.brandname);
        }
    }
}
