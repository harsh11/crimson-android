package thecrimson.co.in.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ProductDetailFragment;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.ProductDataPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;

public class ProductSubTypeAdapter extends RecyclerView.Adapter<ProductSubTypeAdapter.MyViewHolder>{
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    CategoryDataPozo categorydata;
    ArrayList<SubCategoryDataPozo> subcategorydata;
    int positionclicked;
    List<ProductDataPozo> productdata;
    public ProductSubTypeAdapter(MainActivity context, CategoryDataPozo categorydata, ArrayList<SubCategoryDataPozo> subcategorydata, int positionclicked, List<ProductDataPozo> productdata) {
        this.context=context;
        this.categorydata=categorydata;
        this.subcategorydata=subcategorydata;
        this.positionclicked=positionclicked;
        this.productdata=productdata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.producsubtype_adapter, parent, false);

        return new ProductSubTypeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if(productdata!=null) {

            if(productdata.get(position).getDiscount().equals("0%")){

                holder.price.setVisibility(View.GONE);
                holder.discount.setVisibility(View.GONE);
            }else {
                holder.price.setVisibility(View.VISIBLE);
                holder.discount.setVisibility(View.VISIBLE);
            }

            Picasso.get().load(productdata.get(position).getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                    holder.prod_image.setBackgroundDrawable(ob);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    holder.prod_image.setBackgroundDrawable(placeHolderDrawable);

                }
            });
            holder.name.setText(productdata.get(position).getProductName());

            holder.type.setText(productdata.get(position).getCatname());
            holder.brand.setText(productdata.get(position).getBrandname());
            holder.newprice.setText("Rs " + productdata.get(position).getSalePrice());
            holder.price.setText("Rs " + productdata.get(position).getRetailPrice());
            holder.discount.setText("("+productdata.get(position).getDiscount() + " " + "off"+")");

        }

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = "productsubtype";
                fragment = new ProductDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("tag",tag);
                bundle.putSerializable("categorydata",categorydata);
                bundle.putSerializable("subcategorydata",subcategorydata);
                bundle.putInt("productid",productdata.get(position).getProductId());
                bundle.putInt("position",positionclicked);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if(productdata!=null) {
            return productdata.size();
        }

        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout backgroundlayout;
        ImageView prod_image;
        TextView name;
        TextView type;
        TextView brand;
        TextView newprice;
        TextView price;
        TextView discount;
        public MyViewHolder(View itemView) {
            super(itemView);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
            prod_image=itemView.findViewById(R.id.prod_image);
            name=itemView.findViewById(R.id.name);
            type=itemView.findViewById(R.id.type);
            brand=itemView.findViewById(R.id.brand);
            newprice=itemView.findViewById(R.id.newprice);
            price=itemView.findViewById(R.id.price);
            discount=itemView.findViewById(R.id.discount);
        }
    }
}
