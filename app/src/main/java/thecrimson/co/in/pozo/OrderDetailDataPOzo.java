package thecrimson.co.in.pozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailDataPOzo {

    @SerializedName("item")
    @Expose
    private List<OrderDetailData> item = null;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("saving")
    @Expose
    private String saving;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("gst")
    @Expose
    private List<OrderDetailGstPozo> gst;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;

    public List<OrderDetailData> getItem() {
        return item;
    }

    public void setItem(List<OrderDetailData> item) {
        this.item = item;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSaving() {
        return saving;
    }

    public void setSaving(String saving) {
        this.saving = saving;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public List<OrderDetailGstPozo> getGst() {
        return gst;
    }

    public void setGst(List<OrderDetailGstPozo> gst) {
        this.gst = gst;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

}
