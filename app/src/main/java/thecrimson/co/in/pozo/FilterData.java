package thecrimson.co.in.pozo;

import java.util.List;

public class FilterData {

    List<Integer> brand_id;
    int customer_id;
    List<Integer> price_range;
    int category;
    int subcategory;

    public List<Integer> getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(List<Integer> brand_id) {
        this.brand_id = brand_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public List<Integer> getPrice_range() {
        return price_range;
    }

    public void setPrice_range(List<Integer> price_range) {
        this.price_range = price_range;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(int subcategory) {
        this.subcategory = subcategory;
    }
}
