package thecrimson.co.in.pozo;

public class SearchData {

    int customer_id;
    String searchdata;

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getSearchdata() {
        return searchdata;
    }

    public void setSearchdata(String searchdata) {
        this.searchdata = searchdata;
    }
}
