package thecrimson.co.in.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ProductDetailFragment;
import thecrimson.co.in.pozo.PromotionalBannerDataPozo;
import thecrimson.co.in.pozo.PromotionalProductData1Pozo;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.MyViewHolder>{
    List<PromotionalProductData1Pozo> data;
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    PromotionalBannerDataPozo banerdata;
    public PromotionAdapter(List<PromotionalProductData1Pozo> fullproductdata, MainActivity context, PromotionalBannerDataPozo banerdata) {
        this.data=fullproductdata;
        this.context=context;
        this.banerdata=banerdata;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewall_adapter, parent, false);

        return new PromotionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if(data.get(position).getDiscount().equals("0%")){

            holder.discount.setVisibility(View.GONE);
            holder.price.setVisibility(View.GONE);
        }else {

            holder.discount.setVisibility(View.VISIBLE);
            holder.price.setVisibility(View.VISIBLE);
        }

        Picasso.get().load(data.get(position).getImage()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                holder.image.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        holder.name.setText(data.get(position).getProductName());

        holder.type.setText(data.get(position).getCatname());
        holder.brand.setText(data.get(position).getBrandname());
        holder.newprice.setText("Rs " + data.get(position).getSalePrice());
        holder.price.setText("Rs " + data.get(position).getRetailPrice());
        holder.discount.setText("("+data.get(position).getDiscount() + " " + "off"+")");

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String tag = "promotion";
//
//                fragment = new ProductDetailFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("tag",tag);
//                bundle.putInt("productid",data.get(position).getProductId());
//                bundle.putSerializable("bannerdata",banerdata);
//                fragment.setArguments(bundle);
//                fragmentmanager = context.getSupportFragmentManager();
//                fragmentTransaction = fragmentmanager.beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_layout, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView type;
        TextView brand;
        TextView newprice;
        TextView price;
        TextView discount;
        LinearLayout backgroundlayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
            type=itemView.findViewById(R.id.type);
            brand=itemView.findViewById(R.id.brand);
            newprice=itemView.findViewById(R.id.newprice);
            price=itemView.findViewById(R.id.price);
            discount=itemView.findViewById(R.id.discount);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
        }
    }
}
