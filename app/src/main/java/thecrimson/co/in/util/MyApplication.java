package thecrimson.co.in.util;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

import okhttp3.OkHttpClient;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor("crimson-auth", "p@ssw0rd1111"))
                .build();

        AndroidNetworking.initialize(getApplicationContext(),client);
    }
}
