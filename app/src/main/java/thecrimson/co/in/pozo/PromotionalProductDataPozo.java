package thecrimson.co.in.pozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromotionalProductDataPozo {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("banner_id")
    @Expose
    private String bannerId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("sub_category")
    @Expose
    private String subCategory;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("catname")
    @Expose
    private String catname;
    @SerializedName("catslug")
    @Expose
    private String catslug;
    @SerializedName("subcatname")
    @Expose
    private String subcatname;
    @SerializedName("subcatslug")
    @Expose
    private String subcatslug;
    @SerializedName("product")
    @Expose
    private List<PromotionalProductData1Pozo> product = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCatslug() {
        return catslug;
    }

    public void setCatslug(String catslug) {
        this.catslug = catslug;
    }

    public String getSubcatname() {
        return subcatname;
    }

    public void setSubcatname(String subcatname) {
        this.subcatname = subcatname;
    }

    public String getSubcatslug() {
        return subcatslug;
    }

    public void setSubcatslug(String subcatslug) {
        this.subcatslug = subcatslug;
    }

    public List<PromotionalProductData1Pozo> getProduct() {
        return product;
    }

    public void setProduct(List<PromotionalProductData1Pozo> product) {
        this.product = product;
    }
}
