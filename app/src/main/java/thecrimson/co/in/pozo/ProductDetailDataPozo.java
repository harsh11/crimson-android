package thecrimson.co.in.pozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class ProductDetailDataPozo {

    @SerializedName("product_id")
    @Expose
    private int productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("brand_id")
    @Expose
    private int brandId;
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private int subcategoryId;
    @SerializedName("short_descp")
    @Expose
    private String shortDescp;
    @SerializedName("long_descp")
    @Expose
    private String longDescp;
    @SerializedName("retail_price")
    @Expose
    private int retailPrice;
    @SerializedName("gst")
    @Expose
    private String gst;
    @SerializedName("total_visitors")
    @Expose
    private int totalVisitors;
    @SerializedName("new_arrival")
    @Expose
    private int newArrival;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("meta_tag")
    @Expose
    private String metaTag;
    @SerializedName("meta_key")
    @Expose
    private String metaKey;
    @SerializedName("meta_title")
    @Expose
    private String metaTitle;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("catname")
    @Expose
    private String catname;
    @SerializedName("catslug")
    @Expose
    private String catslug;
    @SerializedName("subcatname")
    @Expose
    private String subcatname;
    @SerializedName("subcatslug")
    @Expose
    private String subcatslug;
    @SerializedName("brandname")
    @Expose
    private String brandname;
    @SerializedName("share_slug")
    @Expose
    private String share_slug;
    @SerializedName("brandslug")
    @Expose
    private String brandslug;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sale_price")
    @Expose
    private int salePrice;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("gst_amount")
    @Expose
    private float gstAmount;
    @SerializedName("total_price")
    @Expose
    private float totalPrice;

    int updatedsaleprice;
    int updatedretailprice;
    float updatedtotalprice;
    float updatedgstamount;
    int quantity;
    HashMap<String,List<Float>> map;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(int subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getShortDescp() {
        return shortDescp;
    }

    public void setShortDescp(String shortDescp) {
        this.shortDescp = shortDescp;
    }

    public String getLongDescp() {
        return longDescp;
    }

    public void setLongDescp(String longDescp) {
        this.longDescp = longDescp;
    }

    public int getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(int retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public int getTotalVisitors() {
        return totalVisitors;
    }

    public void setTotalVisitors(int totalVisitors) {
        this.totalVisitors = totalVisitors;
    }

    public int getNewArrival() {
        return newArrival;
    }

    public void setNewArrival(int newArrival) {
        this.newArrival = newArrival;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getMetaTag() {
        return metaTag;
    }

    public void setMetaTag(String metaTag) {
        this.metaTag = metaTag;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getCatslug() {
        return catslug;
    }

    public void setCatslug(String catslug) {
        this.catslug = catslug;
    }

    public String getSubcatname() {
        return subcatname;
    }

    public void setSubcatname(String subcatname) {
        this.subcatname = subcatname;
    }

    public String getSubcatslug() {
        return subcatslug;
    }

    public void setSubcatslug(String subcatslug) {
        this.subcatslug = subcatslug;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getBrandslug() {
        return brandslug;
    }

    public void setBrandslug(String brandslug) {
        this.brandslug = brandslug;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(int salePrice) {
        this.salePrice = salePrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public float getGstAmount() {
        return gstAmount;
    }

    public void setGstAmount(float gstAmount) {
        this.gstAmount = gstAmount;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getUpdatedsaleprice() {
        return updatedsaleprice;
    }

    public void setUpdatedsaleprice(int updatedsaleprice) {
        this.updatedsaleprice = updatedsaleprice;
    }

    public int getUpdatedretailprice() {
        return updatedretailprice;
    }

    public void setUpdatedretailprice(int updatedretailprice) {
        this.updatedretailprice = updatedretailprice;
    }

    public float getUpdatedtotalprice() {
        return updatedtotalprice;
    }

    public void setUpdatedtotalprice(float updatedtotalprice) {
        this.updatedtotalprice = updatedtotalprice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getUpdatedgstamount() {
        return updatedgstamount;
    }

    public void setUpdatedgstamount(float updatedgstamount) {
        this.updatedgstamount = updatedgstamount;
    }

    public HashMap<String, List<Float>> getMap() {
        return map;
    }

    public void setMap(HashMap<String, List<Float>> map) {
        this.map = map;
    }

    public String getShare_slug() {
        return share_slug;
    }

    public void setShare_slug(String share_slug) {
        this.share_slug = share_slug;
    }
}
