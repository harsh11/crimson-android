package thecrimson.co.in.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;

public class ForgetPasswordActivity extends AppCompatActivity implements Validator.ValidationListener,View.OnClickListener {
    @Email
    EditText ed_email;
    TextView tv_submit;
    TextView tv_createacount;
    private Validator validator;
    ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initview();
        clickListener();
    }

    private void clickListener() {

        validator.setValidationListener(this);
        tv_submit.setOnClickListener(this);
        tv_createacount.setOnClickListener(this);
    }

    private void initview() {
        validator = new Validator(this);
        ed_email=findViewById(R.id.ed_email);
        tv_submit=findViewById(R.id.tv_submit);
        tv_createacount=findViewById(R.id.tv_createacount);
        progressbar=findViewById(R.id.progressbar);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/LatoSemibold.ttf");
        tv_createacount.setTypeface(custom_font);
    }

    @Override
    public void onValidationSucceeded() {

        if (!NetworkUtil.isNetworkConnected(ForgetPasswordActivity.this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(ForgetPasswordActivity.this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }


    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);
        Data data = new Data();
        data.setEmail(ed_email.getText().toString());

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        Log.e("jsonstring",jsonstring);

        AndroidNetworking.post(Constant.Url+"forgot_password")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Toast.makeText(ForgetPasswordActivity.this,message,Toast.LENGTH_SHORT).show();
                            ed_email.setText("");

                            finish();

                        }else if(code!=null && code.equals("500")){

                            Toast.makeText(ForgetPasswordActivity.this,message,Toast.LENGTH_SHORT).show();
                            ed_email.setText("");
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(ForgetPasswordActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_submit:

                validator.validate();
                break;

            case R.id.tv_createacount:

                Intent in = new Intent(ForgetPasswordActivity.this,SignUpActivity.class);
                startActivity(in);
                finish();
                break;
        }
    }
}
