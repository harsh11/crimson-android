package thecrimson.co.in.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.PromotionAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data2;
import thecrimson.co.in.pozo.PromotionalBannerDataPozo;
import thecrimson.co.in.pozo.PromotionalProductData1Pozo;
import thecrimson.co.in.pozo.PromotionalProductDataPozo;
import thecrimson.co.in.pozo.PromotionalProductPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

@SuppressLint("ValidFragment")
public class PromotionFragment extends Fragment {
    private PromotionalBannerDataPozo banerdata;
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    private RecyclerView recycleview;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    ProgressBar progressbar;
    ImageView image;
    @SuppressLint("ValidFragment")
    public PromotionFragment(PromotionalBannerDataPozo banerdata) {
        // Required empty public constructor
        this.banerdata = banerdata;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_promotion, container, false);
        initview(view);
        bindevent();
        clicklistener();
        if (!NetworkUtil.isNetworkConnected(context)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }

        return view;
    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);

        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Data2 data = new Data2();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setSlug(banerdata.getSlug());

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"promotion-banner-pro")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            PromotionalProductPozo datum = gson.fromJson(response.toString(), PromotionalProductPozo.class);

                            List<PromotionalProductDataPozo> product = datum.getData();
                            List<PromotionalProductData1Pozo> fullproductdata = new ArrayList<>();

                            for(int i=0;i<product.size();i++){

                                List<PromotionalProductData1Pozo> productdata = product.get(i).getProduct();
                                fullproductdata.addAll(productdata);

                            }

                            PromotionAdapter adapter = new PromotionAdapter(fullproductdata,context,banerdata);
                            recycleview.setAdapter(adapter);


                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clicklistener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }

    private void bindevent() {

        txtToolbar.setText(banerdata.getTilte());

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);


        Picasso.get().load(banerdata.getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                image.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

                image.setBackgroundDrawable(placeHolderDrawable);
            }
        });


    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview=view.findViewById(R.id.recycleview);
        progressbar=view.findViewById(R.id.progressbar);
        image=view.findViewById(R.id.image);
        recycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context= (MainActivity) context;
    }


}
