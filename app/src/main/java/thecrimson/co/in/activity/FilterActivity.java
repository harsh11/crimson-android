package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.adapter.FilterAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.BrandDataPozo;
import thecrimson.co.in.pozo.BrandPozo;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.FilterData;
import thecrimson.co.in.pozo.PriceDataPozo;
import thecrimson.co.in.pozo.PricePozo;
import thecrimson.co.in.pozo.ProductDataPozo;
import thecrimson.co.in.pozo.ProductPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class FilterActivity extends AppCompatActivity {
    RecyclerView recycleview;
    ImageView close;
    ProgressBar progressbar;
    View priceview;
    View brandview;
    TextView brand;
    TextView price;
    LinearLayout pricelayout;
    LinearLayout brandlayout;
    TextView applyfilter;
    private int position;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    CategoryDataPozo categorydata;
    ArrayList<SubCategoryDataPozo> subcategorydata;
    public static ArrayList<ProductDataPozo> productdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initview();
        Intent in = getIntent();
        if(in!=null){

            position = in.getIntExtra("position",-1);
            categorydata = (CategoryDataPozo) in.getSerializableExtra("categorydata");
            subcategorydata = (ArrayList<SubCategoryDataPozo>) in.getSerializableExtra("subcategorydata");
        }
        if (!NetworkUtil.isNetworkConnected(this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCallingBrand();
        }
        bindevent();
        clickListener();
    }

    private void ApiCallingBrand() {

        progressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(Constant.Url+"brand")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            Gson gson = new Gson();
                            BrandPozo datum = gson.fromJson(response.toString(), BrandPozo.class);

                            List<BrandDataPozo> branddata = datum.getData();
                            FilterAdapter adapter = new FilterAdapter(FilterActivity.this,branddata,null);
                            recycleview.setAdapter(adapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(FilterActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clickListener() {

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        pricelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                brandview.setVisibility(View.GONE);
                priceview.setVisibility(View.VISIBLE);
                price.setTextColor(getResources().getColor(R.color.colorwhite));
                brand.setTextColor(getResources().getColor(R.color.transparentwhite));

                FilterAdapter adapter = new FilterAdapter(FilterActivity.this,null,null);
                recycleview.setAdapter(adapter);
                if (!NetworkUtil.isNetworkConnected(FilterActivity.this)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                } else {
                    ApiCallingPrice();
                }


            }
        });

        brandlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                brandview.setVisibility(View.VISIBLE);
                priceview.setVisibility(View.GONE);
                price.setTextColor(getResources().getColor(R.color.transparentwhite));
                brand.setTextColor(getResources().getColor(R.color.colorwhite));
                FilterAdapter adapter = new FilterAdapter(FilterActivity.this,null,null);
                recycleview.setAdapter(adapter);
                if (!NetworkUtil.isNetworkConnected(FilterActivity.this)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                } else {
                    ApiCallingBrand();
                }

            }
        });

        applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gson gson = new Gson();
                String branddata = SharedPreference.getSharedPreferenceString(FilterActivity.this,Constant.branddata,"");
                String pricedata = SharedPreference.getSharedPreferenceString(FilterActivity.this,Constant.pricedata,"");

                Type type = new TypeToken<List<FilterAdapter.ObjectItem>>(){}.getType();
                List<FilterAdapter.ObjectItem> brandlist = gson.fromJson(branddata,type);
                List<FilterAdapter.ObjectItem> pricelist = gson.fromJson(pricedata,type);

                List<Integer> sendbranddata = new ArrayList<>();
                List<Integer> sendpricedata = new ArrayList<>();

                for(int i=0;i<brandlist.size();i++){

                    if(brandlist.get(i).isSelected()==true){

                        sendbranddata.add(brandlist.get(i).getId());
                    }
                }

                if(pricelist!=null) {

                    for (int i = 0; i < pricelist.size(); i++) {

                        if (pricelist.get(i).isSelected() == true) {

                            sendpricedata.add(pricelist.get(i).getId());
                        }
                    }

                }

                String userid = SharedPreference.getSharedPreferenceString(FilterActivity.this,Constant.userid,"");

                FilterData data = new FilterData();
                data.setCustomer_id(Integer.parseInt(userid));
                data.setCategory(categorydata.getId());
                data.setSubcategory(subcategorydata.get(position).getId());
                if(sendbranddata.size()!=0) {
                    data.setBrand_id(sendbranddata);
                }

                if(sendpricedata!=null && sendpricedata.size()!=0){
                    data.setPrice_range(sendpricedata);
                }

                String jsonstring = gson.toJson(data);
                if (!NetworkUtil.isNetworkConnected(FilterActivity.this)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                } else {
                    ApiCalling(jsonstring);
                }



            }
        });
    }

    private void ApiCalling(String jsonstring) {

        AndroidNetworking.post(Constant.Url+"product-filter")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            SharedPreference.setSharedPreferenceString(FilterActivity.this,Constant.branddata,"");
                            SharedPreference.setSharedPreferenceString(FilterActivity.this,Constant.pricedata,"");
                            Gson gson = new Gson();
                            ProductPozo data = gson.fromJson(response.toString(), ProductPozo.class);

                            productdata = data.getData();

                            Intent in =  new Intent(FilterActivity.this,MainActivity.class);
                            in.putExtra("position",position);
                            in.putExtra("tag","filter");
                            in.putExtra("categorydata",categorydata);
                            in.putExtra("subcategorydata",subcategorydata);
                            startActivity(in);
                            finish();


                        }else if(code!=null && code.equals("500")){

                            SharedPreference.setSharedPreferenceString(FilterActivity.this,Constant.branddata,"");
                            SharedPreference.setSharedPreferenceString(FilterActivity.this,Constant.pricedata,"");
                            //Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                            Intent in =  new Intent(FilterActivity.this,MainActivity.class);
                            in.putExtra("position",position);
                            in.putExtra("tag","filter");
                            in.putExtra("categorydata",categorydata);
                            in.putExtra("subcategorydata",subcategorydata);
                            startActivity(in);
                            finish();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(FilterActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void ApiCallingPrice() {

        progressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.get(Constant.Url+"pricelist")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            Gson gson = new Gson();
                            PricePozo data = gson.fromJson(response.toString(), PricePozo.class);

                            List<PriceDataPozo> pricedata = data.getData();
                            FilterAdapter adapter = new FilterAdapter(FilterActivity.this,null,pricedata);
                            recycleview.setAdapter(adapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(FilterActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(FilterActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void bindevent() {


        brandview.setVisibility(View.VISIBLE);
        priceview.setVisibility(View.GONE);
        price.setTextColor(getResources().getColor(R.color.transparentwhite));

    }

    private void initview() {
        recycleview=findViewById(R.id.recycleview);
        close=findViewById(R.id.close);
        progressbar=findViewById(R.id.progressbar);
        brandview=findViewById(R.id.brandview);
        priceview=findViewById(R.id.priceview);
        brand=findViewById(R.id.brand);
        price=findViewById(R.id.price);
        applyfilter=findViewById(R.id.applyfilter);
        pricelayout=findViewById(R.id.pricelayout);
        brandlayout=findViewById(R.id.brandlayout);
        recycleview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

    }
}
