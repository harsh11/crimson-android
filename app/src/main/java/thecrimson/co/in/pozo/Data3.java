package thecrimson.co.in.pozo;

public class Data3 {

    int customer_id;
    int top_trend;

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getTop_trend() {
        return top_trend;
    }

    public void setTop_trend(int top_trend) {
        this.top_trend = top_trend;
    }
}
