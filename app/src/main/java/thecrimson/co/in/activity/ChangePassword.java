package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class ChangePassword extends AppCompatActivity implements Validator.ValidationListener,View.OnClickListener {
    ProgressBar progressbar;
    @NotEmpty
    EditText ed_old_password;
    @Password
    EditText ed_new_password;
    @ConfirmPassword
    EditText ed_confrm_password;
    TextView tv_change_password;
    private Validator validator;
    ImageView backbuton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initview();
        clickListener();
    }

    private void clickListener() {

        validator.setValidationListener(this);
        tv_change_password.setOnClickListener(this);
        backbuton.setOnClickListener(this);
    }

    private void initview() {

        progressbar=findViewById(R.id.progressbar);
        ed_old_password=findViewById(R.id.ed_old_password);
        ed_new_password=findViewById(R.id.ed_new_password);
        ed_confrm_password=findViewById(R.id.ed_confrm_password);
        tv_change_password=findViewById(R.id.tv_change_password);
        backbuton=findViewById(R.id.backbuton);
        validator = new Validator(this);
    }

    @Override
    public void onValidationSucceeded() {

        if (!NetworkUtil.isNetworkConnected(this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }

    }

    private void ApiCalling() {


        progressbar.setVisibility(View.VISIBLE);
        Data data = new Data();

        String userid = SharedPreference.getSharedPreferenceString(this,Constant.userid,"");
        data.setId(Integer.parseInt(userid));
        data.setOldpassword(ed_old_password.getText().toString());
        data.setNewpassword(ed_new_password.getText().toString());

        Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"change-password")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Toast.makeText(ChangePassword.this,message,Toast.LENGTH_SHORT).show();
                            finish();

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(ChangePassword.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(ChangePassword.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_change_password:
                validator.validate();
                break;

            case R.id.backbuton:
                Intent in = new Intent(ChangePassword.this,MainActivity.class);
                startActivity(in);
                break;
        }
    }
}
