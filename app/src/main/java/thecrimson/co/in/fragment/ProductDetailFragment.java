package thecrimson.co.in.fragment;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.activity.SendEnquiryActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.ProductDetailDataPozo;
import thecrimson.co.in.pozo.ProductDetailPozo;
import thecrimson.co.in.pozo.PromotionalBannerDataPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.pozo.TrendingDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.SharedPreference;

@SuppressLint("ValidFragment")
public class ProductDetailFragment extends Fragment {
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    TextView increment_quantity;
    TextView quantity;
    TextView decrement_quantity;
    TextView tv_sendenquiry;
    String tag;
    int qty = 1;
    TextView add_to_cart;
    String viewalltag;
    CategoryDataPozo categorydata;
    ArrayList<SubCategoryDataPozo> subcategorydata;
    int productid;
    ProgressBar progressbar;
    TextView name;
    ImageView image;
    TextView type;
    TextView brand;
    TextView newprice;
    TextView price;
    TextView discount;
    TextView gstpercentage;
    TextView total;
    TextView totalvalue;
    TextView brandtag;
    TextView gsttag;
    ImageView facebook;
    ImageView whtsup;
    int positionclicked;
    private ProductDetailDataPozo productdetaildata;
    private int updatedretailprice;
    private float updatedtotalprice;
    private int updatedsaleprice;
    private float updatedgstamount;
    TextView descritiontext;
    ImageView img_more;
    ArrayList<TrendingDataPozo> productdata;
    HashMap<String,List<Float>> map;
    PromotionalBannerDataPozo banerdata;


    @SuppressLint("ValidFragment")
    public ProductDetailFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tag = getArguments().getString("tag");
        categorydata = (CategoryDataPozo) getArguments().getSerializable("categorydata");
        subcategorydata = (ArrayList<SubCategoryDataPozo>) getArguments().getSerializable("subcategorydata");
        productid=getArguments().getInt("productid");
        positionclicked = getArguments().getInt("position");
        viewalltag = getArguments().getString("viewalltag");
        productdata = (ArrayList<TrendingDataPozo>) getArguments().getSerializable("productdata");
        banerdata = (PromotionalBannerDataPozo) getArguments().getSerializable("bannerdata");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_product_detail, container, false);
        initview(view);
        bindevent();
        clickListener();
        ApiCallingProductDetail();
        return view;
    }

    private void ApiCallingProductDetail() {


        progressbar.setVisibility(View.VISIBLE);

        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Data data = new Data();
        data.setProduct_id(productid);
        data.setCustomer_id(Integer.parseInt(userid));

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"productdetail")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            gsttag.setVisibility(View.VISIBLE);
                            brandtag.setVisibility(View.VISIBLE);

                            ProductDetailPozo datum = gson.fromJson(response.toString(), ProductDetailPozo.class);
                            productdetaildata = datum.getData();

                            if(productdetaildata.getDiscount().equals("0%")){

                                discount.setVisibility(View.GONE);
                                price.setVisibility(View.GONE);
                            }else {
                                discount.setVisibility(View.VISIBLE);
                                price.setVisibility(View.VISIBLE);
                            }

                            Picasso.get().load(productdetaildata.getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                    BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                                    image.setBackgroundDrawable(ob);
                                }

                                @Override
                                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {
                                    image.setBackgroundDrawable(placeHolderDrawable);
                                }
                            });
                            name.setText(productdetaildata.getProductName());

                            type.setText(productdetaildata.getCatname());
                            brand.setText(productdetaildata.getBrandname());
                            newprice.setText("Rs " + productdetaildata.getSalePrice());
                            price.setText("Rs " + productdetaildata.getRetailPrice());
                            discount.setText("("+productdetaildata.getDiscount() + " " + "off"+")");
                            gstpercentage.setText(productdetaildata.getGst());
                            total.setText("Total: "+productdetaildata.getSalePrice()+" + "+productdetaildata.getGst()+"=");
                            totalvalue.setText("Rs "+productdetaildata.getTotalPrice());
                            descritiontext.setText(productdetaildata.getShortDescp());

                            updatedretailprice=productdetaildata.getRetailPrice();
                            updatedtotalprice=productdetaildata.getTotalPrice();
                            updatedsaleprice=productdetaildata.getSalePrice();
                            updatedgstamount=productdetaildata.getGstAmount();
                            qty=1;

                            if(productdetaildata.getShortDescp().equalsIgnoreCase(productdetaildata.getLongDescp())){

                                img_more.setVisibility(View.GONE);
                            }


                        }else if(code!=null && code.equals("500")){

                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button

                if(tag.equalsIgnoreCase("productsubtype")){
                    fragment = new ProductSubType();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("categorydata",categorydata);
                    bundle.putSerializable("subcategorydata",subcategorydata);
                    bundle.putInt("position",positionclicked);
                    fragment.setArguments(bundle);
                    fragmentmanager = context.getSupportFragmentManager();
                    fragmentTransaction = fragmentmanager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment);
                    fragmentTransaction.commit();
                    fragmentmanager.popBackStack();
                }else if(tag.equalsIgnoreCase("home")){
                    fragment = new HomeFragment();
                    fragmentmanager = context.getSupportFragmentManager();
                    fragmentTransaction = fragmentmanager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment);
                    fragmentTransaction.commit();
                    fragmentmanager.popBackStack();

                }else if(tag.equalsIgnoreCase("viewallfragment")){

                    fragment = new ViewAllFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("viewalltag",viewalltag);
                    bundle.putSerializable("productdata",productdata);
                    fragment.setArguments(bundle);
                    fragmentmanager = context.getSupportFragmentManager();
                    fragmentTransaction = fragmentmanager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment);
                    fragmentTransaction.commit();
                    fragmentmanager.popBackStack();
                }else if(tag.equalsIgnoreCase("promotion")){

                    fragment = new PromotionFragment(banerdata);
                    fragmentmanager = context.getSupportFragmentManager();
                    fragmentTransaction = fragmentmanager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //String url = "http://www.crimson.com/"+productdetaildata.getCatname()+"/"+productdetaildata.getSubcatname()+"/"+productdetaildata.getProductName();
                String url = productdetaildata.getShare_slug();
                boolean facebookAppFound = false;
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, url);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(url));

                PackageManager pm = context.getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.packageName).contains("com.facebook.katana")) {
                        final ActivityInfo activityInfo = app.activityInfo;
                        final ComponentName name = new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setComponent(name);
                        facebookAppFound = true;
                        break;
                    }
                }
                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + url;
                    shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                }
                context.startActivity(shareIntent);

            }
        });

        whtsup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //String url = "http://www.crimson.com/"+productdetaildata.getCatname()+"/"+productdetaildata.getSubcatname()+"/"+productdetaildata.getProductName();

                String url = productdetaildata.getShare_slug();
                PackageManager pm = context.getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");

                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    waIntent.putExtra(Intent.EXTRA_TEXT,  url);
                    context.startActivity(Intent
                            .createChooser(waIntent, context.getString(R.string.share_intent_title)));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(context, context.getString(R.string.share_whatsapp_not_instaled),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        increment_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    qty=qty+1;
                    quantity.setText(String.valueOf(qty));

                    updatedretailprice=productdetaildata.getRetailPrice()*qty;
                    updatedtotalprice = productdetaildata.getTotalPrice()*qty;
                    updatedsaleprice = productdetaildata.getSalePrice()*qty;
                    updatedgstamount=productdetaildata.getGstAmount()*qty;

                total.setText("Total: "+updatedsaleprice+" + "+productdetaildata.getGst()+"=");
                totalvalue.setText("Rs "+updatedtotalprice);

            }
        });

        decrement_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(qty<=1){

                    Toast.makeText(context,"Quantity should not be less than 1",Toast.LENGTH_SHORT).show();

                }else {
                    qty = qty - 1;
                    quantity.setText(String.valueOf(qty));
                    updatedretailprice=productdetaildata.getRetailPrice()*qty;
                    updatedtotalprice = productdetaildata.getTotalPrice()*qty;
                    updatedsaleprice = productdetaildata.getSalePrice()*qty;
                    updatedgstamount=productdetaildata.getGstAmount()*qty;

                    total.setText("Total: "+updatedsaleprice+" + "+productdetaildata.getGst()+"=");
                    totalvalue.setText("Rs "+updatedtotalprice);

                }

            }
        });

        img_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (img_more.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.ic_expand_more_black_24dp).getConstantState()){
                    descritiontext.setText(productdetaildata.getLongDescp());
                    img_more.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
                }else if (img_more.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.ic_expand_less_black_24dp).getConstantState()){
                    descritiontext.setText(productdetaildata.getShortDescp());
                    img_more.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                }

            }
        });


        tv_sendenquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context,SendEnquiryActivity.class);
                in.putExtra("productid",productdetaildata.getProductId());
                startActivity(in);
            }
        });

        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean flag=true;
                String addtocartdata = SharedPreference.getSharedPreferenceString(context, Constant.addtocart, "");

//                if(addtocartdata!=null && addtocartdata.equals("[]")||addtocartdata.equals("")){
//                    List<Float> value = new ArrayList<>();
//                    value.add(productdetaildata.getGstAmount());
//                    map.put(productdetaildata.getGst(),value);
//
//                }else {
//
//                    Gson gson = new Gson();
//                    Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();
//                    List<ProductDetailDataPozo> data = gson.fromJson(addtocartdata, type);
//
//                    for(int i=0;i<data.size();i++){
//
//                        if(data.get(i).getGst().equals(productdetaildata.getGst())){
//
//                            flag=false;
//                            map = data.get(i).getMap();
//
//                                List<Float> value = map.get(productdetaildata.getGst());
//                                value.add(productdetaildata.getGstAmount());
//                                map.put(productdetaildata.getGst(), value);
//
//
//
//                        }
//                    }
//
//
//                    if(flag==true){
//
//                        List<Float> value = new ArrayList<>();
//                        value.add(productdetaildata.getGstAmount());
//                        map.put(productdetaildata.getGst(),value);
//                    }
//                }

                ProductDetailDataPozo data = new ProductDetailDataPozo();
                data.setProductId(productdetaildata.getProductId());
                data.setProductName(productdetaildata.getProductName());
                data.setImage(productdetaildata.getImage());
                data.setCatname(productdetaildata.getCatname());
                data.setBrandname(productdetaildata.getBrandname());
                data.setRetailPrice(productdetaildata.getRetailPrice());
                data.setSalePrice(productdetaildata.getSalePrice());
                data.setDiscount(productdetaildata.getDiscount());
                data.setGst(productdetaildata.getGst());
                data.setTotalPrice(productdetaildata.getTotalPrice());
                data.setUpdatedretailprice(updatedretailprice);
                data.setUpdatedsaleprice(updatedsaleprice);
                data.setUpdatedtotalprice(updatedtotalprice);
                data.setQuantity(qty);
                data.setGst(productdetaildata.getGst());
                data.setGstAmount(productdetaildata.getGstAmount());
                data.setUpdatedgstamount(updatedgstamount);
                data.setMap(map);

                storedatatolocal(data);
                context.showBadgeCount();

//                fragment = new CartFragment();
//                fragmentmanager = context.getSupportFragmentManager();
//                fragmentTransaction = fragmentmanager.beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_layout, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();

            }
        });
    }

    private void storedatatolocal(ProductDetailDataPozo data) {

        String addedtag="";

        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();
        String addtocartdata = SharedPreference.getSharedPreferenceString(context,Constant.addtocart,"");


        if(addtocartdata!=null && !addtocartdata.equals("")){
            List<ProductDetailDataPozo> productdata = gson.fromJson(addtocartdata, type);

            for(int i=0;i<productdata.size();i++){

                if(data.getProductId()==productdata.get(i).getProductId()){
                    addedtag = "added";
                    break;
                }
            }

            if(addedtag!=null && !addedtag.equals("")){
                Toast.makeText(context,"This product is already added in cart",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(context,"Product added in cart",Toast.LENGTH_SHORT).show();
                productdata.add(data);
                String jsonstring = gson.toJson(productdata);
                SharedPreference.setSharedPreferenceString(context,Constant.addtocart,jsonstring);
            }

        }else {
            Toast.makeText(context,"Product added in cart",Toast.LENGTH_SHORT).show();
            List<ProductDetailDataPozo> detaildata = new ArrayList<>();
            detaildata.add(data);
            String jsonstring = gson.toJson(detaildata);

            SharedPreference.setSharedPreferenceString(context,Constant.addtocart,jsonstring);
        }


    }



    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        decrement_quantity=view.findViewById(R.id.decrement_quantity);
        quantity=view.findViewById(R.id.quantity);
        increment_quantity=view.findViewById(R.id.increment_quantity);
        add_to_cart=view.findViewById(R.id.add_to_cart);
        tv_sendenquiry=view.findViewById(R.id.tv_sendenquiry);
        progressbar=view.findViewById(R.id.progressbar);
        name=view.findViewById(R.id.name);
        image=view.findViewById(R.id.image);
        type=view.findViewById(R.id.type);
        brand=view.findViewById(R.id.brand);
        newprice=view.findViewById(R.id.newprice);
        price=view.findViewById(R.id.price);
        discount=view.findViewById(R.id.discount);
        gstpercentage=view.findViewById(R.id.gstpercentage);
        total=view.findViewById(R.id.total);
        totalvalue=view.findViewById(R.id.totalvalue);
        whtsup=view.findViewById(R.id.whtsup);
        facebook=view.findViewById(R.id.facebook);
        brandtag=view.findViewById(R.id.brandtag);
        gsttag=view.findViewById(R.id.gsttag);
        img_more=view.findViewById(R.id.img_more);
        descritiontext=view.findViewById(R.id.descritiontext);
        map=new HashMap<>();
    }

    private void bindevent() {

        txtToolbar.setText("");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;
    }


}
