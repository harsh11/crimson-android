package thecrimson.co.in.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ProductSubType;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder>{
    List<SubCategoryDataPozo> subcategorydata;
    MainActivity context;
    public int row_index=-1;
    int positionclicked;
    CategoryDataPozo categorydata;
    ProductSubType productSubType;

    public SubCategoryAdapter(CategoryDataPozo categorydata, List<SubCategoryDataPozo> subcategorydata, MainActivity context, int positionclicked, ProductSubType productSubType) {
        this.subcategorydata=subcategorydata;
        this.context=context;
        this.positionclicked=positionclicked;
        this.categorydata=categorydata;
        this.productSubType=productSubType;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subcategry_adapter, parent, false);

        return new SubCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.subcategry_name.setText(subcategorydata.get(position).getName());


        holder.subcategry_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                row_index=position;
                notifyDataSetChanged();
                productSubType.changeViallColor();
                if (!NetworkUtil.isNetworkConnected(context)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                } else {
                    productSubType.ApiCallingProduct(position);
                }



            }
        });

        if(row_index==-1){
            if(position==positionclicked){

                holder.subcategry_name.setTextColor(context.getResources().getColor(R.color.colorwhite));
            }else {
                holder.subcategry_name.setTextColor(context.getResources().getColor(R.color.colorlightwhite));
            }
        }else {
            if(row_index==position){

                holder.subcategry_name.setTextColor(context.getResources().getColor(R.color.colorwhite));
            }else {

                holder.subcategry_name.setTextColor(context.getResources().getColor(R.color.colorlightwhite));
            }
        }

    }

    @Override
    public int getItemCount() {
        return subcategorydata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subcategry_name;
        public MyViewHolder(View itemView) {
            super(itemView);
            subcategry_name=itemView.findViewById(R.id.subcategry_name);
        }
    }
}
