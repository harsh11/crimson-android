package thecrimson.co.in.util;

public class Constant {

    public static final String Url = "https://www.thecrimson.co.in/api/";
    public static final String NO_INTERNET = "Oops!! No Internet Connection";
    public static final String userid= "userid";
    public static final String fullname= "fullname";
    public static final String email= "email";
    public static final String contact_no= "contact_no";
    public static final String address_line1= "address_line1";
    public static final String address_line2= "address_line2";
    public static final String city= "city";
    public static final String state= "state";
    public static final String pincode= "pincode";
    public static final String company_name= "company_name";
    public static final String customer_type= "customer_type";
    public static final String gstin= "gstin";
    public static final String addtocart= "addtocart";
    public static final String sercheddata= "sercheddata";
    public static final String branddata= "branddata";
    public static final String pricedata= "pricedata";
    public static final String fcmToken= "fcmToken";
}
