package thecrimson.co.in.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.ProductTypeAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.pozo.SubCategoryPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;

@SuppressLint("ValidFragment")
public class ProductTypeFragment extends Fragment {
    Toolbar toolbar;
    TextView txtToolbar;
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    RecyclerView recycleview;
    CategoryDataPozo categorydata;
    ProgressBar progressbar;
    private ArrayList<SubCategoryDataPozo> subcategorydata;
    ImageView categoryimage;


    @SuppressLint("ValidFragment")
    public ProductTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        categorydata = (CategoryDataPozo) getArguments().getSerializable("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_product_type, container, false);

        initview(view);
        bindevent();
        clickListner();

            if (!NetworkUtil.isNetworkConnected(context)) {
                String message = Constant.NO_INTERNET;
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            } else {
                ApiCalling();
            }

        return view;
    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);

        Data data = new Data();
        data.setParent_id(categorydata.getId());

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"category")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            SubCategoryPozo datum = gson.fromJson(response.toString(), SubCategoryPozo.class);

                           subcategorydata = datum.getData();

                            ProductTypeAdapter adapter = new ProductTypeAdapter(context,subcategorydata,categorydata);
                            recycleview.setAdapter(adapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clickListner() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }

    private void bindevent() {

        txtToolbar.setText(categorydata.getName());

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);


        Picasso.get().load(categorydata.getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);
                categoryimage.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

                categoryimage.setBackgroundDrawable(placeHolderDrawable);
            }
        });


    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview=view.findViewById(R.id.recycleview);
        progressbar=view.findViewById(R.id.progressbar);
        categoryimage=view.findViewById(R.id.categoryimage);
        recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;
    }

}
