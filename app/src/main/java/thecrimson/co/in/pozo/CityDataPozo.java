package thecrimson.co.in.pozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityDataPozo {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("state_id")
    @Expose
    private String state_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }
}
