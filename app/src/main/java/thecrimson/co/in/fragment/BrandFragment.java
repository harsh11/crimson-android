package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.AdapterBrand;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.BrandDataPozo;

public class BrandFragment extends Fragment {

    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    private RecyclerView recycleview;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    List<BrandDataPozo> branddata;


    public BrandFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        branddata = (List<BrandDataPozo>) getArguments().getSerializable("branddata");

        for(int i=0;i<branddata.size();i++){

            branddata.get(i).getName();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_brand, container, false);
        initview(view);
        bindevent();
        clickListener();
        return view;
    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }


    private void bindevent() {

        txtToolbar.setText("BRANDS");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);

        AdapterBrand adapter = new AdapterBrand(branddata,context);
        recycleview.setAdapter(adapter);

    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview=view.findViewById(R.id.recycleview);
        recycleview.setLayoutManager(new GridLayoutManager(context,2));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       this.context= (MainActivity) context;
    }

}
