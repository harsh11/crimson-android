package thecrimson.co.in.pozo;

public class Gst {
    String gst;
    float gst_amount;

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public float getGst_amount() {
        return gst_amount;
    }

    public void setGst_amount(float gst_amount) {
        this.gst_amount = gst_amount;
    }
}
