package thecrimson.co.in.pozo;

public class Data2 {

    int customer_id;
    int new_arrival;
    String slug;
    int id;

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getNew_arrival() {
        return new_arrival;
    }

    public void setNew_arrival(int new_arrival) {
        this.new_arrival = new_arrival;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
