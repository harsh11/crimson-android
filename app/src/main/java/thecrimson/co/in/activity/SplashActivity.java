package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import thecrimson.co.in.R;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.SharedPreference;

public class SplashActivity extends AppCompatActivity {
    private static int POSTDELAYTIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                String userid = SharedPreference.getSharedPreferenceString(SplashActivity.this,Constant.userid,"");

                if(userid!=null && !userid.equals("")){
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);

                }

                finish();
            }
        }, POSTDELAYTIME);
    }
}
