package thecrimson.co.in.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.BrandDataPozo;
import thecrimson.co.in.pozo.MenuCategoryPozo;
import thecrimson.co.in.pozo.MenuSubCategoryPozo;
import thecrimson.co.in.pozo.TrendingDataPozo;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;

    parent parentInterface;
    chlid childInterface;


    private List<Item> data;

    public ExpandableListAdapter(List<Item> data, parent parentInterface, chlid childInterface) {

        this.data = data;
        this.childInterface=childInterface;
        this.parentInterface=parentInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        Context context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (45 * dp);
        int subItemPaddingTopAndBottom = (int) (4 * dp);

        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.list_header, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);

                return header;

            case CHILD:

                TextView itemTextView = new TextView(context);
                itemTextView.setPadding(subItemPaddingLeft, subItemPaddingTopAndBottom, 0, subItemPaddingTopAndBottom);
                itemTextView.setTextColor(context.getResources().getColor(R.color.colorwhite));
                itemTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                itemTextView.setLayoutParams(
                        new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                return new RecyclerView.ViewHolder(itemTextView) {
                };

        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Item item = data.get(position);


       switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.text);

//                if(item.invisibleChildren.size()>0){
//                    itemController.btn_expand_toggle.setVisibility(View.VISIBLE);
//                }else {
//                    itemController.btn_expand_toggle.setVisibility(View.GONE);
//                }

                if (item.invisibleChildren == null) {
                    //itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);

                    itemController.header_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            parentInterface.setparentclicklistener(position,item.text);
                            if (item.invisibleChildren == null) {
                                item.invisibleChildren = new ArrayList<Item>();
                                int count = 0;
                                int pos = data.indexOf(itemController.refferalItem);
                                while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                    item.invisibleChildren.add(data.remove(pos + 1));
                                    count++;


                                }
                                notifyItemRangeRemoved(pos + 1, count);
                                itemController.btn_expand_toggle.setImageResource(R.drawable.add);

                            } else {
                                int pos = data.indexOf(itemController.refferalItem);
                                int index = pos + 1;
                                for (Item i : item.invisibleChildren) {
                                    data.add(index, i);
                                    index++;
                                }
                                notifyItemRangeInserted(pos + 1, index - pos - 1);
                                itemController.btn_expand_toggle.setImageResource(R.drawable.ic_remove_white_24dp);

                                item.invisibleChildren = null;
                            }
                        }
                    });


                } else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.add);
                    itemController.btn_expand_toggle.setVisibility(View.VISIBLE);

                    itemController.header_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            parentInterface.setparentclicklistener(position,item.text);

                            if (item.invisibleChildren == null) {
                                item.invisibleChildren = new ArrayList<Item>();
                                int count = 0;
                                int pos = data.indexOf(itemController.refferalItem);
                                while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                    item.invisibleChildren.add(data.remove(pos + 1));
                                    count++;


                                }
                                notifyItemRangeRemoved(pos + 1, count);
                                itemController.btn_expand_toggle.setImageResource(R.drawable.add);

                            } else {
                                int pos = data.indexOf(itemController.refferalItem);
                                int index = pos + 1;
                                for (Item i : item.invisibleChildren) {
                                    data.add(index, i);
                                    index++;
                                }
                                notifyItemRangeInserted(pos + 1, index - pos - 1);
                                itemController.btn_expand_toggle.setImageResource(R.drawable.ic_remove_white_24dp);

                                item.invisibleChildren = null;
                            }
                        }
                    });
                }

                itemController.btn_expand_toggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;


                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.add);

                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.ic_remove_white_24dp);

                            item.invisibleChildren = null;
                        }
                    }
                });
                break;
            case CHILD:

                TextView itemTextView = (TextView) holder.itemView;
                itemTextView.setText(data.get(position).text);

                itemTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        childInterface.setchildclicklistener(position,item.categorydata,item.subcategorydata,item.text,item.branddata,item.productdata,item.product_type);

//                        item.invisibleChildren = new ArrayList<Item>();
//                        int count = 0;
//                        int pos = data.indexOf(itemController.refferalItem);
//                        while (data.size() > pos + 1 && data.get(pos + 1).type == CHILD) {
//                            item.invisibleChildren.add(data.remove(pos + 1));
//                            count++;
//
//
//                        }
//                        notifyItemRangeRemoved(pos + 1, count);
//                        itemController.btn_expand_toggle.setImageResource(R.drawable.add);

                    }
                });


                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public View view;
        public Item refferalItem;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.header_title);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.btn_expand_toggle);
            view=(View)itemView.findViewById(R.id.view);
        }
    }

    public static class Item {
        public int type;
        public String text;
        public MenuCategoryPozo categorydata;
        public List<Item> invisibleChildren;
        List<MenuSubCategoryPozo> subcategorydata;
        List<BrandDataPozo> branddata;
        ArrayList<TrendingDataPozo> productdata;
        String product_type;


        public Item(int type, String text, MenuCategoryPozo categorydata, List<MenuSubCategoryPozo> subcategorydata, List<BrandDataPozo> branddata, ArrayList<TrendingDataPozo> productdata,String product_type) {
            this.type = type;
            this.text = text;
            this.categorydata=categorydata;
            this.subcategorydata=subcategorydata;
            this.branddata=branddata;
            this.productdata=productdata;
            this.product_type=product_type;

        }
    }


   public interface parent {
        public void setparentclicklistener(int pos, String text);
    }

    public interface chlid {
        public void setchildclicklistener(int pos, MenuCategoryPozo categorydata, List<MenuSubCategoryPozo> subcategorydata, String text, List<BrandDataPozo> branddata,ArrayList<TrendingDataPozo> productdata,String product_type);
    }



}
