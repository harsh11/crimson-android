package thecrimson.co.in.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.MyOrderDetailFragment;
import thecrimson.co.in.pozo.OrderListDataPozo;

public class MyOrderListAdapter extends RecyclerView.Adapter<MyOrderListAdapter.MyViewHolder>{
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    ArrayList<OrderListDataPozo> orderlsitdata;
    public MyOrderListAdapter(MainActivity context, ArrayList<OrderListDataPozo> orderlsitdata) {
        this.context=context;
        this.orderlsitdata=orderlsitdata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_order_list_adapter, parent, false);

        return new MyOrderListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new MyOrderDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("orderdata",orderlsitdata.get(position));
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        holder.orderno.setText(orderlsitdata.get(position).getOrderNumber());
        holder.date.setText(orderlsitdata.get(position).getOrderDate());
        holder.amount.setText("Rs "+orderlsitdata.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return orderlsitdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout backgroundlayout;
        TextView orderno;
        TextView date;
        TextView amount;
        public MyViewHolder(View itemView) {
            super(itemView);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
            orderno=itemView.findViewById(R.id.orderno);
            date=itemView.findViewById(R.id.date);
            amount=itemView.findViewById(R.id.amount);
        }
    }
}
