package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class SendEnquiryActivity extends AppCompatActivity implements View.OnClickListener,Validator.ValidationListener {
    ImageView close_image;
    @NotEmpty
    EditText ed_email;
    @NotEmpty
    EditText ed_message;
    TextView tv_send;
    private Validator validator;
    private int productid;
    ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_enquiry);
        getIntentData();
        initview();
        clickListener();
    }

    private void getIntentData() {

        Intent in = getIntent();
        if(in!=null){

            productid = in.getIntExtra("productid",-1);
        }
    }


    private void clickListener() {

        close_image.setOnClickListener(this);
        tv_send.setOnClickListener(this);
        validator.setValidationListener(this);

    }

    private void initview() {
        validator = new Validator(this);

        close_image=findViewById(R.id.close_image);
        ed_email=findViewById(R.id.ed_email);
        ed_message=findViewById(R.id.ed_message);
        tv_send=findViewById(R.id.tv_send);
        progressbar=findViewById(R.id.progressbar);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){

            case R.id.close_image:

                finish();
                break;


            case R.id.tv_send:

                validator.validate();

                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        if (!NetworkUtil.isNetworkConnected(this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }
    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);
        String email = ed_email.getText().toString();
        String message = ed_message.getText().toString();
        String userid = SharedPreference.getSharedPreferenceString(this,Constant.userid,"");

        Data data = new Data();
        data.setEmail(email);
        data.setMsg(message);
        data.setUser_id(Integer.parseInt(userid));
        data.setProduct_id(productid);

        Gson gson = new Gson();
        String jsonstring = gson.toJson(data);


        AndroidNetworking.post(Constant.Url+"product-enq")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Toast.makeText(SendEnquiryActivity.this,message,Toast.LENGTH_SHORT).show();
                            finish();

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(SendEnquiryActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(SendEnquiryActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
