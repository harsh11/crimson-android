package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.TextView;

import thecrimson.co.in.R;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.SharedPreference;

public class LogOutActivity extends AppCompatActivity implements OnClickListener {
    TextView tv_logout;
    TextView tv_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_out);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initview();
        clickListener();
    }

    private void clickListener() {

        tv_logout.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);

    }

    private void initview() {

        tv_logout=findViewById(R.id.tv_logout);
        tv_cancel=findViewById(R.id.tv_cancel);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()){

            case R.id.tv_logout:
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.userid,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.fullname,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.email,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.contact_no,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.address_line1,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.address_line2,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.city,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.state,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.pincode,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.company_name,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.customer_type,"");
                SharedPreference.setSharedPreferenceString(LogOutActivity.this,Constant.gstin,"");

                Intent in = new Intent(LogOutActivity.this,LoginActivity.class);
                startActivity(in);
                finish();
                break;

            case R.id.tv_cancel:
                Intent in1 = new Intent(LogOutActivity.this,MainActivity.class);
                startActivity(in1);
                finish();
                break;
        }
    }
}
