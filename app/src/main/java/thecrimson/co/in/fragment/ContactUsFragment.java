package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.ContactusData;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;

public class ContactUsFragment extends Fragment implements Validator.ValidationListener{
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    @NotEmpty
    EditText ed_name;
    @Email
    EditText ed_email;
    @NotEmpty
    EditText ed_phn_number;
    @NotEmpty
    EditText ed_subject;
    @NotEmpty
    EditText ed_message;
    TextView tv_send;
    Fragment fragment;
    ProgressBar progressbar;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    private Validator validator;

    public ContactUsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact_us, container, false);
        initview(view);
        bindevent();
        clicklistener();
        return view;
    }

    private void clicklistener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button

                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();


            }
        });

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validator.validate();

            }
        });


    }

    private void ApiCalling(String jsonstring) {

        progressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(Constant.Url+"contact")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){
                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();

                            fragment = new HomeFragment();
                            fragmentmanager = context.getSupportFragmentManager();
                            fragmentTransaction = fragmentmanager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_layout, fragment);
                            fragmentTransaction.commit();
                            fragmentmanager.popBackStack();

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void bindevent() {
        txtToolbar.setText("CONTACT US");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

    }

    private void initview(View view) {
        validator = new Validator(this);
        validator.setValidationListener(this);
        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        ed_name=view.findViewById(R.id.ed_name);
        ed_email=view.findViewById(R.id.ed_email);
        ed_phn_number=view.findViewById(R.id.ed_phn_number);
        ed_subject=view.findViewById(R.id.ed_subject);
        ed_message=view.findViewById(R.id.ed_message);
        tv_send=view.findViewById(R.id.tv_send);
        progressbar=view.findViewById(R.id.progressbar);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;

    }


    @Override
    public void onValidationSucceeded() {
        String name = ed_name.getText().toString();
        String email = ed_email.getText().toString();
        String mobileno = ed_phn_number.getText().toString();
        String subject = ed_subject.getText().toString();
        String message = ed_message.getText().toString();

        BigInteger biginteger = new BigInteger(mobileno);

        Gson gson = new Gson();
        ContactusData data = new ContactusData();
        data.setName(name);
        data.setEmail(email);
        data.setPhone_no(biginteger);
        data.setSubject(subject);
        data.setMessage(message);
        data.setType(1);

        String jsonstring = gson.toJson(data);

        if (!NetworkUtil.isNetworkConnected(context)) {
            String internetmessage = Constant.NO_INTERNET;
            Toast.makeText(context,internetmessage,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling(jsonstring);
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
