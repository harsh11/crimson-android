package thecrimson.co.in.pozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailData {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_tittle")
    @Expose
    private String productTittle;
    @SerializedName("product_short_description")
    @Expose
    private String productShortDescription;
    @SerializedName("product_long_description")
    @Expose
    private String productLongDescription;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("retail_price")
    @Expose
    private String retailPrice;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("product_brand")
    @Expose
    private String product_brand;
    @SerializedName("product_category")
    @Expose
    private String product_category;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductTittle() {
        return productTittle;
    }

    public void setProductTittle(String productTittle) {
        this.productTittle = productTittle;
    }

    public String getProductShortDescription() {
        return productShortDescription;
    }

    public void setProductShortDescription(String productShortDescription) {
        this.productShortDescription = productShortDescription;
    }

    public String getProductLongDescription() {
        return productLongDescription;
    }

    public void setProductLongDescription(String productLongDescription) {
        this.productLongDescription = productLongDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProduct_brand() {
        return product_brand;
    }

    public void setProduct_brand(String product_brand) {
        this.product_brand = product_brand;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }
}
