package thecrimson.co.in.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.PromotionFragment;
import thecrimson.co.in.pozo.PromotionalBannerDataPozo;


public class SliderPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    MainActivity activity;
    List<PromotionalBannerDataPozo> bannerdata;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;

    public SliderPagerAdapter(MainActivity activity, List<PromotionalBannerDataPozo> bannerdata) {
        this.activity = activity;
        this.bannerdata = bannerdata;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.layout_slider, container, false);
        final ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);
        Picasso.get().load(bannerdata.get(position).getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

               // im_slider.setImageBitmap(bitmap);
                BitmapDrawable ob = new BitmapDrawable(activity.getResources(), bitmap);
                im_slider.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

                im_slider.setBackgroundDrawable(placeHolderDrawable);
            }
        });

        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new PromotionFragment(bannerdata.get(position));
                fragmentmanager = activity.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return bannerdata.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

}
