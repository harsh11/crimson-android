package thecrimson.co.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import thecrimson.co.in.R;

public class PlaceOrderActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView txtToolbar;
    private int ordernumber;
    private String date;
    TextView orderno;
    TextView oredrdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        initview();
        getIntentdata();
        bindevent();
        settingdata();
        clicklistener();
    }

    private void settingdata() {

        orderno.setText("Order No: "+String.valueOf(ordernumber));
        oredrdate.setText("Order Placed Succesfully on "+date);
    }

    private void getIntentdata() {

        Intent in = getIntent();
        if(in!=null){

            ordernumber = in.getIntExtra("order_number",-1);
            date = in.getStringExtra("date");
        }
    }

    private void clicklistener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                Intent in = new Intent(PlaceOrderActivity.this,MainActivity.class);
                startActivity(in);
                finish();
            }
        });

    }

    private void bindevent() {

        txtToolbar.setText("Order No: "+ordernumber);

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initview() {

        toolbar = findViewById(R.id.toolbar);
        txtToolbar=findViewById(R.id.toolbar_title);
        oredrdate=findViewById(R.id.date);
        orderno=findViewById(R.id.orderno);
    }
}
