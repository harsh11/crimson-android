package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import thecrimson.co.in.R;
import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.MyOrderListAdapter;
import thecrimson.co.in.pozo.CartData;
import thecrimson.co.in.pozo.OrderListDataPozo;
import thecrimson.co.in.pozo.OrderListPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;


public class MyOrderListFragment extends Fragment {
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    RecyclerView recycleview;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    ProgressBar progressbar;
    LinearLayout tablecolum;
    public MyOrderListFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_order_list, container, false);
        initView(view);
        bindevent();
        clicklISTENER();
        if (!NetworkUtil.isNetworkConnected(context)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }
        return view;
    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);

        final CartData data = new CartData();
        data.setCustomer_id(Integer.parseInt(SharedPreference.getSharedPreferenceString(context,Constant.userid,"")));

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"orderlist")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            tablecolum.setVisibility(View.VISIBLE);
                            Gson gson = new Gson();
                            OrderListPozo datum = gson.fromJson(response.toString(), OrderListPozo.class);

                            ArrayList<OrderListDataPozo> orderlsitdata = datum.getData();

                            MyOrderListAdapter adapter = new MyOrderListAdapter(context,orderlsitdata);
                            recycleview.setAdapter(adapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void clicklISTENER() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }

    private void bindevent() {

        txtToolbar.setText("MY ORDER LIST");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);


    }

    private void initView(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview = view.findViewById(R.id.recycleview);
        progressbar = view.findViewById(R.id.progressbar);
        tablecolum = view.findViewById(R.id.tablecolum);
        recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;

    }


}
