package thecrimson.co.in.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ProductTypeFragment;
import thecrimson.co.in.pozo.CategoryDataPozo;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder>{
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    List<CategoryDataPozo> categorydata;
    public ProductAdapter(MainActivity context, List<CategoryDataPozo> categorydata) {
        this.context=context;
        this.categorydata=categorydata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_adapter, parent, false);

        return new ProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {


        final CategoryDataPozo data = categorydata.get(position);

        Picasso.get().load(data.getImage()).placeholder(R.drawable.placehoderimage).into(holder.categry_image);
        holder.category_name.setText(data.getName());

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new ProductTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data",data);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return categorydata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout backgroundlayout;
        CircleImageView categry_image;
        TextView category_name;
        public MyViewHolder(View itemView) {
            super(itemView);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
            categry_image=itemView.findViewById(R.id.categry_image);
            category_name=itemView.findViewById(R.id.category_name);
        }
    }
}
