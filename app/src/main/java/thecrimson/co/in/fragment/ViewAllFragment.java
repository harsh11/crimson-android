package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.ViewAllAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data4;
import thecrimson.co.in.pozo.TrendingDataPozo;
import thecrimson.co.in.pozo.TrendingPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;


public class ViewAllFragment extends Fragment {

    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    private RecyclerView recycleview;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    String tag;
    ArrayList<TrendingDataPozo> productdata;
    private int brandid;
    ProgressBar progressbar;
    private String brandname;


    public ViewAllFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        tag = getArguments().getString("viewalltag");
        productdata = (ArrayList<TrendingDataPozo>) getArguments().getSerializable("productdata");
        brandid = getArguments().getInt("brandid");
        brandname = getArguments().getString("brandname");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_view_all, container, false);
        initview(view);
        bindevent();
        clickListener();
        if(tag!=null && tag.equals("brand")){

            if (!NetworkUtil.isNetworkConnected(context)) {
                String message = Constant.NO_INTERNET;
                Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
            } else {

                ApiCalling(brandid);
            }
        }else {


            ViewAllAdapter adapter = new ViewAllAdapter(context, tag, productdata);
            recycleview.setAdapter(adapter);
        }
        return view;
    }

    private void ApiCalling(int brandid) {

        progressbar.setVisibility(View.VISIBLE);
        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Data4 data = new Data4();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setBrand_id(brandid);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                       progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                             productdata = datum.getData();

                            ViewAllAdapter adapter = new ViewAllAdapter(context, tag, productdata);
                            recycleview.setAdapter(adapter);



                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                       progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview=view.findViewById(R.id.recycleview);
        progressbar=view.findViewById(R.id.progressbar);
        recycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
    }

    private void bindevent() {

        if(tag!=null && tag.equalsIgnoreCase("toptrending")){
            txtToolbar.setText("TOP TRENDING");
        }else if(tag!=null && tag.equalsIgnoreCase("newarrival")){
            txtToolbar.setText("NEW ARRIVAL");
        }else if(tag!=null && tag.equals("brand")){
            txtToolbar.setText(brandname);
        }else {
            txtToolbar.setText("");
        }

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);




    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       this.context= (MainActivity) context;
    }


}
