package thecrimson.co.in.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.activity.PlaceOrderActivity;
import thecrimson.co.in.adapter.CartAdapter;
import thecrimson.co.in.adapter.GstAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.CartData;
import thecrimson.co.in.pozo.Gst;
import thecrimson.co.in.pozo.GstPozo;
import thecrimson.co.in.pozo.Item;
import thecrimson.co.in.pozo.ProductDetailDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class CartFragment extends Fragment {

    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    Fragment fragment;
    TextView retailamount;
    TextView savingamount;
    TextView saleamount;
    TextView grandtotal;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    RecyclerView recycleview;
    LinearLayout price_layout;
    RecyclerView gstrecycleview;
    List<GstPozo> gstlist;
    List<GstPozo> adaptergstlist;
    TextView place_order;
    ProgressBar progressbar;
    TextView countinue_shopping;
    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_cart, container, false);
        initview(view);
        gettingcartdata();
        settingdata();
        bindevent();
        clickListener();
        return view;
    }

    public void settingdata() {

        gstlist=new ArrayList<>();
        adaptergstlist=new ArrayList<>();

        float retailprice = 0;
        float saleprice = 0;
        float saving=0;
        float gstprice10=0;
        float totalprice=0;
        float gstprice5=0;

        String addtocartdata = SharedPreference.getSharedPreferenceString(context, Constant.addtocart, "");

        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();
        List<ProductDetailDataPozo> data = gson.fromJson(addtocartdata, type);

        if(data!=null) {

            for (int i = 0; i < data.size(); i++) {

                retailprice = retailprice + data.get(i).getUpdatedretailprice();
                saleprice = saleprice + data.get(i).getUpdatedsaleprice();
                saving = retailprice - saleprice;
                totalprice = totalprice + data.get(i).getUpdatedtotalprice();



                if(data.get(i).getGst().equals("10%")){

                    gstprice10 = gstprice10 + data.get(i).getUpdatedgstamount();

                }else if(data.get(i).getGst().equals("5%")){

                    gstprice5 = gstprice5+data.get(i).getUpdatedgstamount();

                }


//                HashMap<String, List<Float>> map = data.get(i).getMap();
//
//                if(map!=null) {
//
//                    for (Map.Entry<String, List<Float>> entry : map.entrySet()) {
//                        String key = entry.getKey();
//                        List<Float> value = entry.getValue();
//                        float totalvaue = 0.0f;
//                        for (int j = 0; j < value.size(); j++) {
//                            totalvaue = totalvaue + value.get(j);
//                        }
//
//                        GstPozo gstpozo = new GstPozo();
//                        gstpozo.setPercentage(key);
//                        gstpozo.setAmount(totalvaue);
//                        gstlist.add(gstpozo);
//                    }
//
//                }
            }

            retailamount.setText("Rs "+String.valueOf(retailprice));
            savingamount.setText("Rs "+String.valueOf(saving));
            saleamount.setText("Rs "+String.valueOf(saleprice));
            grandtotal.setText("Rs "+String.valueOf(totalprice));

            GstPozo pozo = new GstPozo();
            pozo.setPercentage("10%");
            pozo.setAmount(gstprice10);
            adaptergstlist.add(pozo);

            GstPozo pozo1 = new GstPozo();
            pozo1.setPercentage("5%");
            pozo1.setAmount(gstprice5);
            adaptergstlist.add(pozo1);


//                    List<Integer> list = new ArrayList<>();
//
//                  for(int i=0;i<gstlist.size();i++){
//
//                      for(int j=i+1;j<gstlist.size();j++){
//
//                          if(gstlist.get(i).getPercentage().equals(gstlist.get(j).getPercentage())){
//
//                              list.add(i);
//                              break;
//
//
//                           }
//
//                      }
//                  }
//
//
//                  List<GstPozo> newgstlist = new ArrayList<>();
//                  for(int k=0;k<gstlist.size();k++){
//                      boolean flag = true;
//                     for(int i=0;i<list.size();i++){
//
//                         if(k==list.get(i)){
//                             flag=false;
//                             break;
//                         }
//                     }
//
//                     if(flag==true){
//
//                         newgstlist.add(gstlist.get(k));
//                     }
//
//                  }


            GstAdapter adapter = new GstAdapter(adaptergstlist);
            gstrecycleview.setAdapter(adapter);



        }
    }

    public void gettingcartdata() {

        String addtocartdata = SharedPreference.getSharedPreferenceString(context, Constant.addtocart, "");

        if(addtocartdata!=null && !addtocartdata.equals("")){
            Gson gson = new Gson();
            Type type = new TypeToken<List<ProductDetailDataPozo>>() {
            }.getType();
            List<ProductDetailDataPozo> data = gson.fromJson(addtocartdata, type);


            if (data != null && data.size() > 0) {

                price_layout.setVisibility(View.VISIBLE);
                countinue_shopping.setVisibility(View.VISIBLE);
                CartAdapter adapter = new CartAdapter(context, data, CartFragment.this);
                recycleview.setAdapter(adapter);
            } else if(data.size()==0){
                price_layout.setVisibility(View.GONE);
                countinue_shopping.setVisibility(View.GONE);
                CartAdapter adapter = new CartAdapter(context, data, CartFragment.this);
                recycleview.setAdapter(adapter);
                Toast.makeText(context, "No data available", Toast.LENGTH_SHORT).show();
            }else {

                price_layout.setVisibility(View.GONE);
                countinue_shopping.setVisibility(View.GONE);
                Toast.makeText(context, "No data available", Toast.LENGTH_SHORT).show();
            }

        }


    }

    private void clickListener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button

                    fragment = new HomeFragment();
                    fragmentmanager = context.getSupportFragmentManager();
                    fragmentTransaction = fragmentmanager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment);
                    fragmentTransaction.commit();
                    fragmentmanager.popBackStack();


            }
        });

        countinue_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent in = new Intent(context,MainActivity.class);
               startActivity(in);
               context.finish();
            }
        });

        place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<Item> itemlist = new ArrayList<>();
                List<Gst> gstlist = new ArrayList<>();
                float retailprice = 0;
                float saleprice = 0;
                float saving=0;
                float totalprice=0;

                String addtocartdata = SharedPreference.getSharedPreferenceString(context, Constant.addtocart, "");
                Gson gson = new Gson();
                Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();
                List<ProductDetailDataPozo> data = gson.fromJson(addtocartdata, type);
                CartData cartdata = new CartData();
                cartdata.setCustomer_id(Integer.parseInt(SharedPreference.getSharedPreferenceString(context,Constant.userid,"")));
                for(int i=0;i<data.size();i++){

                    Item item = new Item();
                    item.setProduct_id(data.get(i).getProductId());
                    item.setRetail_price(data.get(i).getUpdatedretailprice());
                    item.setSale_price(data.get(i).getUpdatedsaleprice());
                    item.setDiscount(data.get(i).getDiscount());
                    item.setQty(data.get(i).getQuantity());
                    itemlist.add(item);

                    retailprice = retailprice + data.get(i).getUpdatedretailprice();
                    saleprice = saleprice + data.get(i).getUpdatedsaleprice();
                    saving = retailprice - saleprice;
                    totalprice = totalprice + data.get(i).getUpdatedtotalprice();

//                    HashMap<String, List<Float>> map = data.get(i).getMap();
//
//                    for ( Map.Entry<String, List<Float>> entry : map.entrySet()) {
//                        String key = entry.getKey();
//                        List<Float> value = entry.getValue();
//                        float totalvaue=0.0f;
//                        for (int j=0;j<value.size();j++){
//
//                            totalvaue=totalvaue+value.get(j);
//                        }
//
//                        Gst gst = new Gst();
//                        gst.setGst(key);
//                        gst.setGst_amount(totalvaue);
//                        gstlist.add(gst);
//                    }
                }

                for(int i=0;i<adaptergstlist.size();i++){

                    if(adaptergstlist.get(i).getAmount()!=0.0) {
                        Gst gst = new Gst();
                        gst.setGst(adaptergstlist.get(i).getPercentage());
                        gst.setGst_amount(adaptergstlist.get(i).getAmount());
                        gstlist.add(gst);
                    }
                }

                cartdata.setItem(itemlist);
                cartdata.setTotal_price(retailprice);
                cartdata.setSaving(saving);
                cartdata.setSub_total(saleprice);
                cartdata.setGst(gstlist);
                String abc = new DecimalFormat("##.##").format(totalprice);
                cartdata.setGrand_total(Float.parseFloat(abc));

                String jsonstring = gson.toJson(cartdata);

                Log.e("jsonstring",jsonstring);


                if (!NetworkUtil.isNetworkConnected(context)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                } else {
                    ApiCaling(jsonstring);
                }
            }
        });
    }

    private void ApiCaling(String jsonstring) {

        progressbar.setVisibility(View.VISIBLE);

        AndroidNetworking.post(Constant.Url+"order")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String date;
                    public int order_number;
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                            order_number=response.getInt("order_number");
                            date=response.getString("date");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            SharedPreference.setSharedPreferenceString(context,Constant.addtocart,"");
                            Intent in = new Intent(context,PlaceOrderActivity.class);
                            in.putExtra("order_number",order_number);
                            in.putExtra("date",date);
                            startActivity(in);
                            context.finish();


                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void bindevent() {

        txtToolbar.setText("CART");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));


    }

    private void initview(View view) {
        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview = view.findViewById(R.id.recycleview);
        retailamount = view.findViewById(R.id.retailamount);
        savingamount = view.findViewById(R.id.savingamount);
        saleamount = view.findViewById(R.id.saleamount);
        grandtotal = view.findViewById(R.id.grandtotal);
        gstrecycleview = view.findViewById(R.id.gstrecycleview);
        price_layout = view.findViewById(R.id.price_layout);
        place_order = view.findViewById(R.id.place_order);
        progressbar = view.findViewById(R.id.progressbar);
        countinue_shopping = view.findViewById(R.id.countinue_shopping);
        recycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        gstrecycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       this.context= (MainActivity) context;
    }


}
