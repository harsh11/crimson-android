package thecrimson.co.in.adapter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.ProductSubType;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;

public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.MyViewHolder>{
    MainActivity context;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    ArrayList<SubCategoryDataPozo> subcategorydata;
    CategoryDataPozo categorydata;
    public ProductTypeAdapter(MainActivity context, ArrayList<SubCategoryDataPozo> subcategorydata, CategoryDataPozo categorydata) {
        this.context=context;
        this.subcategorydata=subcategorydata;
        this.categorydata=categorydata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productype_adapter, parent, false);

        return new ProductTypeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.subcategoryname.setText(subcategorydata.get(position).getName());

        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new ProductSubType();
                Bundle bundle = new Bundle();
                bundle.putSerializable("categorydata",categorydata);
                bundle.putSerializable("subcategorydata",subcategorydata);
                bundle.putInt("position",position);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return subcategorydata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout backgroundlayout;
        TextView subcategoryname;
        public MyViewHolder(View itemView) {
            super(itemView);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
            subcategoryname=itemView.findViewById(R.id.subcategoryname);
        }
    }
}
