package thecrimson.co.in.adapter;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder>{
    List<String> serchlist;
    MainActivity mainActivity;
    ProgressBar progressbar;
    Dialog dialog;
    public SearchAdapter(List<String> serchlist, MainActivity mainActivity, ProgressBar progressbar, Dialog dialog) {
        this.serchlist=serchlist;
        this.mainActivity = mainActivity;
        this.progressbar=progressbar;
        this.dialog=dialog;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_search, parent, false);

        return new SearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.serchtext.setText(serchlist.get(position));
        holder.backgroundlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.performSearch(progressbar,serchlist.get(position),dialog);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serchlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serchtext;
        LinearLayout backgroundlayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            serchtext=itemView.findViewById(R.id.serchtext);
            backgroundlayout=itemView.findViewById(R.id.backgroundlayout);
        }
    }
}
