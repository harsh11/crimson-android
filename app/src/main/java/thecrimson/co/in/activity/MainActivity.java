package thecrimson.co.in.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.adapter.ExpandableListAdapter;
import thecrimson.co.in.adapter.NewArrivalAdapter;
import thecrimson.co.in.adapter.SearchAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.adapter.TrendingAdapter;
import thecrimson.co.in.fragment.CartFragment;
import thecrimson.co.in.fragment.ContactUsFragment;
import thecrimson.co.in.fragment.HomeFragment;
import thecrimson.co.in.fragment.MyOrderListFragment;
import thecrimson.co.in.fragment.MyProfileFragment;
import thecrimson.co.in.fragment.NotificationFragment;
import thecrimson.co.in.fragment.ProductDetailFragment;
import thecrimson.co.in.fragment.ProductSubType;
import thecrimson.co.in.fragment.ViewAllFragment;
import thecrimson.co.in.pozo.BrandDataPozo;
import thecrimson.co.in.pozo.BrandPozo;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.Data2;
import thecrimson.co.in.pozo.Data3;
import thecrimson.co.in.pozo.MenuCategoryPozo;
import thecrimson.co.in.pozo.MenuPozo;
import thecrimson.co.in.pozo.MenuSubCategoryPozo;
import thecrimson.co.in.pozo.ProductDetailDataPozo;
import thecrimson.co.in.pozo.SearchData;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.pozo.TrendingDataPozo;
import thecrimson.co.in.pozo.TrendingPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;
import thecrimson.co.in.util.Utilview;

public class MainActivity extends AppCompatActivity implements ExpandableListAdapter.parent, ExpandableListAdapter.chlid{
    RecyclerView recyclerView;
    RecyclerView recyclerView1;
    List<ExpandableListAdapter.Item> category;
    List<ExpandableListAdapter.Item> category1;
    DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    private LayerDrawable cartmenuIcon;
    private int position;
    private String tag;
    CategoryDataPozo categorydata;
    ArrayList<SubCategoryDataPozo> subcategorydata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initview();
        gettingintentData();
        if(tag!=null && tag.equals("filter")){

            fragment = new ProductSubType();
            Bundle bundle = new Bundle();
            bundle.putInt("position",position);
            bundle.putString("tag","filter");
            bundle.putSerializable("categorydata",categorydata);
            bundle.putSerializable("subcategorydata",subcategorydata);
            fragment.setArguments(bundle);
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }else {
            bindevent();
        }

            if (!NetworkUtil.isNetworkConnected(this)) {
                String message = Constant.NO_INTERNET;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            } else {
                ApiCalling();
                brandapicalling();

            }
    }

    private void newarrivalApiCalling() {

        String userid = SharedPreference.getSharedPreferenceString(this,Constant.userid,"");
        Data2 data = new Data2();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setNew_arrival(1);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                            ArrayList<TrendingDataPozo> productdata = datum.getData();
                            ExpandableListAdapter.Item item = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "NEW ARRIVAL", null, null,null,null,"");
                            item.invisibleChildren = new ArrayList<>();
                            for(int i=0;i<productdata.size();i++){

                                item.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, productdata.get(i).getProductName(), null, null,null,productdata,"new_arrival"));

                            }
                            category1.add(item);

                            ExpandableListAdapter.Item item1 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "MY PROFILE", null, null,null,null,"");
                            ExpandableListAdapter.Item item2 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "MY ORDER LIST", null, null,null,null,"");
                            ExpandableListAdapter.Item item3 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "CHANGE PASSWORD", null, null,null,null,"");
                            ExpandableListAdapter.Item item4 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "CONTACT US", null, null,null,null,"");
                            ExpandableListAdapter.Item item5 = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "SIGN OUT", null, null,null,null,"");

                            category1.add(item1);
                            category1.add(item2);
                            category1.add(item3);
                            category1.add(item4);
                            category1.add(item5);

                            recyclerView1.setAdapter(new ExpandableListAdapter(category1, MainActivity.this, MainActivity.this));


                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(MainActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void toptrendingApiCalling() {

        String userid = SharedPreference.getSharedPreferenceString(this,Constant.userid,"");
        Data3 data = new Data3();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setTop_trend(1);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .addStringBody(jsonstring)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                            ArrayList<TrendingDataPozo> productdata = datum.getData();

                            ExpandableListAdapter.Item item = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "TOP TRENDING", null, null,null,null,"");
                            item.invisibleChildren = new ArrayList<>();
                            for(int i=0;i<productdata.size();i++){

                                item.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, productdata.get(i).getProductName(), null, null,null,productdata,"top_trending"));

                            }
                            category1.add(item);
                            newarrivalApiCalling();

                        }else if(code!=null && code.equals("500")){
                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error

                        Toast.makeText(MainActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void gettingintentData() {

        Intent in = getIntent();
        if(in!=null){

            position = in.getIntExtra("position",-1);
            tag = in.getStringExtra("tag");
            categorydata = (CategoryDataPozo) in.getSerializableExtra("categorydata");
            subcategorydata = (ArrayList<SubCategoryDataPozo>) in.getSerializableExtra("subcategorydata");
        }
    }

    private void brandapicalling() {

        AndroidNetworking.post(Constant.Url+"brand")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){


                            Gson gson = new Gson();
                            BrandPozo datum = gson.fromJson(response.toString(), BrandPozo.class);

                            List<BrandDataPozo> branddata = datum.getData();

                            ExpandableListAdapter.Item item = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "BRANDS", null, null,null,null,"");
                            item.invisibleChildren = new ArrayList<>();
                            for(int i=0;i<branddata.size();i++){

                                item.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, branddata.get(i).getName(), null, null,branddata,null,""));

                            }
                            category1.add(item);

                            toptrendingApiCalling();


                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(MainActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void ApiCalling() {

        AndroidNetworking.post(Constant.Url+"categorynav")
                .setContentType("application/json; charset=utf-8")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){
                            Gson gson = new Gson();
                            MenuPozo datum = gson.fromJson(response.toString(), MenuPozo.class);

                            List<MenuCategoryPozo> categorydata = datum.getData();

                            for(int i=0;i<categorydata.size();i++){

                                ExpandableListAdapter.Item item = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, categorydata.get(i).getName(), null, null,null,null,"");
                                item.invisibleChildren = new ArrayList<>();
                                List<MenuSubCategoryPozo> subcategorydata = categorydata.get(i).getSubcat();

                                for(int j=0;j<subcategorydata.size();j++){

                                    item.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, subcategorydata.get(j).getName(), categorydata.get(i), subcategorydata,null,null,""));
                                }

                                category.add(item);

                            }
                            recyclerView.setAdapter(new ExpandableListAdapter(category, MainActivity.this, MainActivity.this));


                        }else if(code!=null && code.equals("500")){
                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(MainActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() != 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void bindevent() {
        fragment = new HomeFragment();
        fragmentmanager = getSupportFragmentManager();
        fragmentTransaction = fragmentmanager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem itemCart = menu.findItem(R.id.menu_cart);

        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        return super.onCreateOptionsMenu(menu);
    }

    public void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();

        String data = SharedPreference.getSharedPreferenceString(this,Constant.addtocart,"");

        List<ProductDetailDataPozo> list = gson.fromJson(data,type);

        if(list!=null && list.size()>0) {

            Utilview.setBadgeCount(this, cartmenuIcon, "" + list.size());
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_notification:
                fragment = new NotificationFragment();
                fragmentmanager = getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;

            case R.id.menu_cart:

                fragment = new CartFragment();
                fragmentmanager = getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;

            case R.id.menu_search:

               showDialog();
                return true;

            case R.id.menu_refresh:
                fragment = new HomeFragment();
                fragmentmanager = getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDialog() {

        final Dialog dialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_search);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;


        dialog.getWindow().setAttributes(lp);

        RecyclerView recycleview = dialog.findViewById(R.id.recycleview);
        final EditText editText= dialog.findViewById(R.id.edittext);
        final ProgressBar progressbar = dialog.findViewById(R.id.progressbar);
        ImageView close= dialog.findViewById(R.id.close);
        recycleview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        String searcheddata = SharedPreference.getSharedPreferenceString(MainActivity.this,Constant.sercheddata,"");
        Type type = new TypeToken<List<String>>(){}.getType();
        Gson gson = new Gson();
        List<String> serchlist = gson.fromJson(searcheddata,type);

        if(serchlist!=null) {

            SearchAdapter adapter = new SearchAdapter(serchlist, MainActivity.this,progressbar,dialog);
            recycleview.setAdapter(adapter);

        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

                    String edtext = editText.getText().toString();
                    performSearch(progressbar,edtext,dialog);
                    return true;
                }
                return false;
            }
        });

        dialog.show();
    }

    public void performSearch(final ProgressBar progressbar, final String edtext, final Dialog dialog) {

        progressbar.setVisibility(View.VISIBLE);

        String userid = SharedPreference.getSharedPreferenceString(this,Constant.userid,"");
        SearchData data = new SearchData();
        data.setCustomer_id(Integer.parseInt(userid));
        data.setSearchdata(edtext);

        final Gson gson = new Gson();
        final String jsonstring = gson.toJson(data);

        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String tag="true";
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            String sercheddata = SharedPreference.getSharedPreferenceString(MainActivity.this, Constant.sercheddata, "");
                            Type type = new TypeToken<List<String>>(){}.getType();
                            if(sercheddata!=null && !sercheddata.equals("")){

                                List<String> serchlist = gson.fromJson(sercheddata, type);
                                for(int i=0;i<serchlist.size();i++){

                                    if(serchlist.get(i).equalsIgnoreCase(edtext)){
                                     tag = "false";
                                    }
                                }
                                if(tag!=null && tag.equalsIgnoreCase("true")){
                                    serchlist.add(edtext);
                                }


                                String jstring = gson.toJson(serchlist);

                                SharedPreference.setSharedPreferenceString(MainActivity.this,Constant.sercheddata,jstring);
                            }else {

                                List<String> searcheddata = new ArrayList<>();
                                searcheddata.add(edtext);

                                String jstring = gson.toJson(searcheddata);

                                SharedPreference.setSharedPreferenceString(MainActivity.this,Constant.sercheddata,jstring);
                            }
                            dialog.dismiss();
                            TrendingPozo datum = gson.fromJson(response.toString(), TrendingPozo.class);
                            ArrayList<TrendingDataPozo> productdata = datum.getData();

                            fragment = new ViewAllFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("productdata",productdata);
                            fragment.setArguments(bundle);
                            fragmentmanager = getSupportFragmentManager();
                            fragmentTransaction = fragmentmanager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_layout, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(MainActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initview() {

        category=new ArrayList<>();
        category1=new ArrayList<>();
        recyclerView=findViewById(R.id.recyclerview);
        recyclerView1=findViewById(R.id.recyclerview1);
        drawer=findViewById(R.id.drawer_layout);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView1.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {

        drawer.closeDrawers();
    }

    @Override
    public void setparentclicklistener(int pos, String text) {

        if(text.equalsIgnoreCase("MY ORDER LIST")){

            drawer.closeDrawers();
            fragment = new MyOrderListFragment();
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if(text.equalsIgnoreCase("MY PROFILE")){

            drawer.closeDrawers();
            fragment = new MyProfileFragment();
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if(text.equalsIgnoreCase("CONTACT US")){

            drawer.closeDrawers();
            fragment = new ContactUsFragment();
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        if(text.equalsIgnoreCase("SIGN OUT")){

            Intent in1 = new Intent(MainActivity.this,LogOutActivity.class);
            startActivity(in1);
            finish();
        }


        if(text.equalsIgnoreCase("CHANGE PASSWORD")){

            drawer.closeDrawers();
            Intent in1 = new Intent(MainActivity.this,ChangePassword.class);
            startActivity(in1);
        }
    }


    @Override
    public void setchildclicklistener(int pos, MenuCategoryPozo categorydata, List<MenuSubCategoryPozo> subcategorydata, String text, List<BrandDataPozo> branddata,ArrayList<TrendingDataPozo> productdata,String product_type) {

        closeDrawer();
        if(categorydata!=null) {
            int position = -1;
            CategoryDataPozo catdata = new CategoryDataPozo();
            catdata.setId(categorydata.getId());
            catdata.setName(categorydata.getName());

            ArrayList<SubCategoryDataPozo> subcatdata = new ArrayList<>();

            for (int i = 0; i < subcategorydata.size(); i++) {

                SubCategoryDataPozo data = new SubCategoryDataPozo();
                data.setId(subcategorydata.get(i).getId());
                data.setName(subcategorydata.get(i).getName());

                subcatdata.add(data);

                if (text.equals(subcategorydata.get(i).getName())) {

                    position = i;
                }
            }
            fragment = new ProductSubType();
            Bundle bundle = new Bundle();
            bundle.putSerializable("categorydata", catdata);
            bundle.putSerializable("subcategorydata", subcatdata);
            bundle.putInt("position", position);
            fragment.setArguments(bundle);
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }else if(branddata!=null){


            fragment = new ViewAllFragment();
            Bundle bundle = new Bundle();
            bundle.putString("viewalltag","brand");
            bundle.putInt("brandid",branddata.get(pos-1).getId());
            bundle.putString("brandname",branddata.get(pos-1).getName());
            fragment.setArguments(bundle);
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }else if(productdata!=null){


            String tag = "home";

            fragment = new ProductDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString("tag",tag);
            if(product_type.equalsIgnoreCase("top_trending")){
                bundle.putInt("productid",productdata.get(pos-2).getProductId());
            }else if(product_type.equalsIgnoreCase("new_arrival")){
                bundle.putInt("productid",productdata.get(pos-3).getProductId());
            }

            fragment.setArguments(bundle);
            fragmentmanager = getSupportFragmentManager();
            fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }


    }
}
