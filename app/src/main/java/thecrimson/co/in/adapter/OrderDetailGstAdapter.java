package thecrimson.co.in.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.OrderDetailGstPozo;

public class OrderDetailGstAdapter extends RecyclerView.Adapter<OrderDetailGstAdapter.MyViewHolder>{
    List<OrderDetailGstPozo> gstlist;

    public OrderDetailGstAdapter(List<OrderDetailGstPozo> gstlist) {

        this.gstlist=gstlist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_gst, parent, false);

        return new OrderDetailGstAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.gstpercentage.setText("GST @ "+gstlist.get(position).getGst());
        holder.gstamount.setText("+ Rs "+String.valueOf(new DecimalFormat("##.##").format(gstlist.get(position).getGst_amount())));
    }

    @Override
    public int getItemCount() {
        return gstlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView gstpercentage;
        TextView gstamount;
        public MyViewHolder(View itemView) {
            super(itemView);
            gstamount=itemView.findViewById(R.id.gstamount);
            gstpercentage=itemView.findViewById(R.id.gstpercentage);
        }
    }
}
