package thecrimson.co.in.util;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("MyFirebaseidservice", "Refreshed token: " + refreshedToken);

        SharedPreference.setSharedPreferenceString(this,Constant.fcmToken,refreshedToken);
    }
}
