package thecrimson.co.in.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.lang.reflect.Type;
import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.fragment.CartFragment;
import thecrimson.co.in.pozo.ProductDetailDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.SharedPreference;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder>{
    MainActivity context;
    List<ProductDetailDataPozo> data;
    CartFragment cartFragment;
    public CartAdapter(MainActivity context, List<ProductDetailDataPozo> data, CartFragment cartFragment) {
        this.context=context;
        this.data=data;
        this.cartFragment=cartFragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_adapter, parent, false);

        return new CartAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if(data!=null){

            if(data.get(position).getDiscount().equals("0%")){

                holder.price.setVisibility(View.GONE);
                holder.discount.setVisibility(View.GONE);
            }else {

                holder.discount.setVisibility(View.VISIBLE);
                holder.price.setVisibility(View.VISIBLE);
            }

            Picasso.get().load(data.get(position).getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                    holder.image.setBackgroundDrawable(ob);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                    holder.image.setBackgroundDrawable(placeHolderDrawable);
                }
            });
            holder.name.setText(data.get(position).getProductName());

            holder.type.setText(data.get(position).getCatname());
            holder.brand.setText(data.get(position).getBrandname());
            holder.newprice.setText("Rs " + data.get(position).getUpdatedsaleprice());
            holder.price.setText("Rs " + data.get(position).getUpdatedretailprice());
            holder.discount.setText("("+data.get(position).getDiscount() + " " + "off"+")");
            holder.quantity.setText(String.valueOf(data.get(position).getQuantity()));

        }

        holder.decrement_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(data.get(position).getQuantity()<=1){

                    Toast.makeText(context,"Quantity should not be less than 1",Toast.LENGTH_SHORT).show();
                }else {

                    int qty=data.get(position).getQuantity()-1;
                    data.get(position).setQuantity(qty);
                    data.get(position).setUpdatedretailprice(data.get(position).getRetailPrice()*qty);
                    data.get(position).setUpdatedsaleprice(data.get(position).getSalePrice()*qty);
                    data.get(position).setUpdatedtotalprice(data.get(position).getTotalPrice()*qty);
                    data.get(position).setUpdatedgstamount(data.get(position).getGstAmount()*qty);

                    String jsonstring = SharedPreference.getSharedPreferenceString(context,Constant.addtocart,"");
                    Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();

                    Gson gson = new Gson();
                    List<ProductDetailDataPozo> productdata = gson.fromJson(jsonstring,type);
                    for(int i =0;i<productdata.size();i++){

                        if(productdata.get(i).getProductId()==data.get(position).getProductId()){

                            productdata.set(i,data.get(position));
                            break;
                        }
                    }

                    String updateddata = gson.toJson(productdata);
                    SharedPreference.setSharedPreferenceString(context,Constant.addtocart,updateddata);
                    notifyDataSetChanged();
                    cartFragment.settingdata();
                }
            }
        });

        holder.incrementquantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int qty=data.get(position).getQuantity()+1;

                data.get(position).setQuantity(qty);
                data.get(position).setUpdatedretailprice(data.get(position).getRetailPrice()*qty);
                data.get(position).setUpdatedsaleprice(data.get(position).getSalePrice()*qty);
                data.get(position).setUpdatedtotalprice(data.get(position).getTotalPrice()*qty);
                data.get(position).setUpdatedgstamount(data.get(position).getGstAmount()*qty);

                String jsonstring = SharedPreference.getSharedPreferenceString(context,Constant.addtocart,"");
                Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();

                Gson gson = new Gson();
                List<ProductDetailDataPozo> productdata = gson.fromJson(jsonstring,type);
                for(int i =0;i<productdata.size();i++){

                    if(productdata.get(i).getProductId()==data.get(position).getProductId()){

                        productdata.set(i,data.get(position));
                        break;
                    }
                }

                String updateddata = gson.toJson(productdata);
                SharedPreference.setSharedPreferenceString(context,Constant.addtocart,updateddata);
                notifyDataSetChanged();
                cartFragment.settingdata();

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                deletealert(position);



            }
        });
    }

    private void deletealert(final int position) {

        final Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.logout_alert);
        d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        Button yes, no;
        yes = (Button) d.findViewById(R.id.btYes);
        no = (Button) d.findViewById(R.id.btCancel);
        yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String jsonstring = SharedPreference.getSharedPreferenceString(context,Constant.addtocart,"");
                Type type = new TypeToken<List<ProductDetailDataPozo>>(){}.getType();

                Gson gson = new Gson();
                List<ProductDetailDataPozo> productdata = gson.fromJson(jsonstring,type);

                for(int i=0;i<productdata.size();i++){

                    if(data.get(position).getProductId()==productdata.get(i).getProductId()){

                        productdata.remove(productdata.get(i));
                        break;
                    }
                }
                String updateddata = gson.toJson(productdata);
                SharedPreference.setSharedPreferenceString(context,Constant.addtocart,updateddata);
                Toast.makeText(context,"Product deleted successfully",Toast.LENGTH_SHORT).show();
                cartFragment.gettingcartdata();
                cartFragment.settingdata();
                d.dismiss();

            }
        });
        no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return -1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView decrement_quantity;
        TextView quantity;
        TextView incrementquantity;
        ImageView image;
        TextView name;
        TextView type;
        TextView brand;
        TextView newprice;
        TextView price;
        TextView discount;
        ImageView delete;
        public MyViewHolder(View itemView) {
            super(itemView);
            decrement_quantity=itemView.findViewById(R.id.decrement_quantity);
            quantity=itemView.findViewById(R.id.quantity);
            incrementquantity=itemView.findViewById(R.id.incrementquantity);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
            type=itemView.findViewById(R.id.type);
            brand=itemView.findViewById(R.id.brand);
            newprice=itemView.findViewById(R.id.newprice);
            price=itemView.findViewById(R.id.price);
            discount=itemView.findViewById(R.id.discount);
            delete=itemView.findViewById(R.id.delete);

        }
    }
}
