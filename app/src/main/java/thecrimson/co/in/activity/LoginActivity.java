package thecrimson.co.in.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.LoginDataPozo;
import thecrimson.co.in.pozo.LoginPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.GPSTracker;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class LoginActivity extends AppCompatActivity implements ValidationListener,View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {
    @Email
    EditText ed_email;
    @NotEmpty
    EditText ed_pasword;
    TextView tv_forgetpasword;
    TextView tv_signin;
    TextView tv_logingoogle;
    TextView tv_createacount;
    ProgressBar progressbar;
    private Validator validator;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled())
        {


            String stringLatitude = String.valueOf(gpsTracker.latitude);
            Log.e("latitude",stringLatitude);


            String stringLongitude = String.valueOf(gpsTracker.longitude);
            Log.e("Longitude",stringLongitude);


            String country = gpsTracker.getCountryName(this);
           // Log.e("country",country);


            String city = gpsTracker.getLocality(this);
           // Log.e("city",city);


            String postalCode = gpsTracker.getPostalCode(this);
            //Log.e("postalcode",postalCode);


            String addressLine = gpsTracker.getAddressLine(this);
            //Log.e("addressline",addressLine);

        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
        initview();
        bindevent();
        clickListener();
    }

    private void bindevent() {


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void clickListener() {
        validator.setValidationListener(this);
        tv_signin.setOnClickListener(this);
        tv_createacount.setOnClickListener(this);
        tv_forgetpasword.setOnClickListener(this);
        tv_logingoogle.setOnClickListener(this);

    }

    private void initview() {

        validator = new Validator(this);

        ed_email=findViewById(R.id.ed_email);
        ed_pasword=findViewById(R.id.ed_pasword);
        tv_forgetpasword=findViewById(R.id.tv_forgetpasword);
        tv_signin=findViewById(R.id.tv_signin);
        tv_logingoogle=findViewById(R.id.tv_logingoogle);
        tv_createacount=findViewById(R.id.tv_createacount);
        progressbar=findViewById(R.id.progressbar);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/LatoSemibold.ttf");
        tv_createacount.setTypeface(custom_font);
    }

    @Override
    public void onValidationSucceeded() {


        if (!NetworkUtil.isNetworkConnected(LoginActivity.this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }


    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);
        Data data = new Data();
        data.setEmail(ed_email.getText().toString());
        data.setPassword(ed_pasword.getText().toString());
        data.setDevice_token(SharedPreference.getSharedPreferenceString(this,Constant.fcmToken,""));

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        Log.e("jsonstring",jsonstring);

        AndroidNetworking.post(Constant.Url+"login")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            LoginPozo datum = gson.fromJson(response.toString(), LoginPozo.class);

                            LoginDataPozo logindata = datum.getData();
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.userid,String.valueOf(logindata.getId()));
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.fullname,logindata.getFullname());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.email,logindata.getEmail());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.contact_no,logindata.getContactNo());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.address_line1,logindata.getAddress());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.address_line2,logindata.getAddressTwo());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.city,logindata.getCity());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.state,logindata.getRegion());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.pincode,logindata.getPostcode());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.company_name,logindata.getCompanyName());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.customer_type,logindata.getCustomerType());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.gstin,logindata.getGsitn());

                            Intent in = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(in);
                            finish();

                        }else if(code!=null && code.equals("500")){

                            ed_email.setText("");
                            ed_pasword.setText("");

                            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_signin:

                validator.validate();
                break;

            case R.id.tv_createacount:

                Intent in = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(in);
                //finish();
                break;

            case R.id.tv_forgetpasword:

                Intent forgetin = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                startActivity(forgetin);
                break;

            case R.id.tv_logingoogle:
                if (!NetworkUtil.isNetworkConnected(LoginActivity.this)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                } else {
                    OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                    if (opr.isDone()) {
                        signoutGoogle();
                        signinGoogle();
                    }else {
                        signinGoogle();
                    }
                }

                break;
        }
    }

    private void signoutGoogle() {

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String id = acct.getId();
            String name = acct.getDisplayName();
            String email = acct.getEmail();
            Uri photourl = acct.getPhotoUrl();
            String idtoken = acct.getIdToken();

            if (!NetworkUtil.isNetworkConnected(LoginActivity.this)) {
                String message = Constant.NO_INTERNET;
                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
            } else {
                ApiCallinggooglelogin(name,email);
            }




        } else {
            Toast.makeText(this, "Error while sign in", Toast.LENGTH_LONG).show();

        }
    }

    private void ApiCallinggooglelogin(String name, String email) {

        progressbar.setVisibility(View.VISIBLE);
        Data data = new Data();
        data.setEmail(email);
        data.setName(name);

        final Gson gson = new Gson();
        String jsonstring = gson.toJson(data);

        Log.e("jsonstring",jsonstring);

        AndroidNetworking.post(Constant.Url+"google-login")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            LoginPozo datum = gson.fromJson(response.toString(), LoginPozo.class);

                            LoginDataPozo logindata = datum.getData();
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.userid,String.valueOf(logindata.getId()));
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.fullname,logindata.getFullname());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.email,logindata.getEmail());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.contact_no,logindata.getContactNo());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.address_line1,logindata.getAddress());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.address_line2,logindata.getAddressTwo());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.city,logindata.getCity());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.state,logindata.getRegion());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.pincode,logindata.getPostcode());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.company_name,logindata.getCompanyName());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.customer_type,logindata.getCustomerType());
                            SharedPreference.setSharedPreferenceString(LoginActivity.this,Constant.gstin,logindata.getGsitn());

                            Intent in = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(in);
                            finish();




                        }else if(code!=null && code.equals("500")){

                            ed_email.setText("");
                            ed_pasword.setText("");

                            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void signinGoogle() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.e("", "onConnectionFailed:" + connectionResult);
    }
}
