package thecrimson.co.in.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.activity.FilterActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.BrandDataPozo;
import thecrimson.co.in.pozo.PriceDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.SharedPreference;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder>{
    FilterActivity filterActivity;
    List<BrandDataPozo> branddata;
    List<PriceDataPozo> pricedata;
    Gson gson;
    private List<FilterAdapter.ObjectItem> selectedlistbrand =new ArrayList<>();
    private List<FilterAdapter.ObjectItem> selectedlistprice =new ArrayList<>();

    public FilterAdapter(FilterActivity filterActivity, List<BrandDataPozo> branddata, List<PriceDataPozo> pricedata) {
        gson=new Gson();
        this.filterActivity=filterActivity;
        this.branddata=branddata;
        this.pricedata=pricedata;

        if(branddata!=null){

            String savedbranddata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.branddata,"");
            if(savedbranddata.equals("") || savedbranddata.equals("[]")) {
                for (BrandDataPozo datum : branddata) {

                    FilterAdapter.ObjectItem objectItme = new FilterAdapter.ObjectItem();
                    objectItme.setPos(branddata.indexOf(datum));
                    objectItme.setSelected(false);
                    objectItme.setId(datum.getId());
                    selectedlistbrand.add(objectItme);

                }

                String jsonstring = gson.toJson(selectedlistbrand);

                SharedPreference.setSharedPreferenceString(filterActivity, Constant.branddata, jsonstring);
            }

        }else if(pricedata!=null){
            String savedpricedata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.pricedata,"");
            if(savedpricedata.equals("") || savedpricedata.equals("[]")) {
                for (PriceDataPozo datum : pricedata) {

                    FilterAdapter.ObjectItem objectItme = new FilterAdapter.ObjectItem();
                    objectItme.setPos(pricedata.indexOf(datum));
                    objectItme.setSelected(false);
                    objectItme.setId(datum.getId());
                    selectedlistprice.add(objectItme);

                }

                String jsonstring = gson.toJson(selectedlistprice);

                SharedPreference.setSharedPreferenceString(filterActivity, Constant.pricedata, jsonstring);

            }

        }


    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_filter, parent, false);

        return new FilterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if(branddata!=null) {
            holder.brandname.setText(branddata.get(position).getName());

            holder.checkBox.setOnCheckedChangeListener(null);

            String branddata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.branddata,"");
            Type type = new TypeToken<List<ObjectItem>>(){}.getType();
            final List<FilterAdapter.ObjectItem> selectedbrand = gson.fromJson(branddata,type);

            if (selectedbrand.get(position) != null && selectedbrand.get(position).isSelected()) {

                holder.checkBox.setChecked(true);

            } else {

                holder.checkBox.setChecked(false);

            }

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    String branddata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.branddata,"");
                    Type type = new TypeToken<List<ObjectItem>>(){}.getType();
                    List<FilterAdapter.ObjectItem> selectedbrand = gson.fromJson(branddata,type);

                    if (b) {

                        selectedbrand.get(position).setSelected(true);
                    } else {

                        selectedbrand.get(position).setSelected(false);
                    }

                    String jsonstring = gson.toJson(selectedbrand);

                    SharedPreference.setSharedPreferenceString(filterActivity,Constant.branddata,jsonstring);
                }
            });

        }else if (pricedata!=null){
            holder.brandname.setText(pricedata.get(position).getValue());

            holder.checkBox.setOnCheckedChangeListener(null);

            String pricedata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.pricedata,"");
            Type type = new TypeToken<List<ObjectItem>>(){}.getType();
            final List<FilterAdapter.ObjectItem> selectedprice = gson.fromJson(pricedata,type);
            if (selectedprice.get(position) != null && selectedprice.get(position).isSelected()) {

                holder.checkBox.setChecked(true);

            } else {

                holder.checkBox.setChecked(false);

            }

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    String pricedata = SharedPreference.getSharedPreferenceString(filterActivity,Constant.pricedata,"");
                    Type type = new TypeToken<List<ObjectItem>>(){}.getType();
                    List<FilterAdapter.ObjectItem> selectedprice = gson.fromJson(pricedata,type);
                    if (b) {

                        selectedprice.get(position).setSelected(true);
                    } else {

                        selectedprice.get(position).setSelected(false);
                    }

                    String jsonstring = gson.toJson(selectedprice);

                    SharedPreference.setSharedPreferenceString(filterActivity,Constant.pricedata,jsonstring);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(branddata!=null) {
            return branddata.size();
        }else if(pricedata!=null){
            return pricedata.size();
        }
        return -1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView brandname;
        CheckBox checkBox;
        public MyViewHolder(View itemView) {
            super(itemView);
            brandname=itemView.findViewById(R.id.brandname);
            checkBox=itemView.findViewById(R.id.checkbox);
        }
    }

    public class ObjectItem {

        int pos;
        boolean selected;
        int id;

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
