package thecrimson.co.in.pozo;

public class OrderDetailGstPozo {

    String gst;
    Float gst_amount;

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public Float getGst_amount() {
        return gst_amount;
    }

    public void setGst_amount(Float gst_amount) {
        this.gst_amount = gst_amount;
    }
}
