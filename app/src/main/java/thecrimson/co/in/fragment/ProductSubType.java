package thecrimson.co.in.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.activity.FilterActivity;
import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.adapter.ProductSubTypeAdapter;
import thecrimson.co.in.adapter.SubCategoryAdapter;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.CategoryDataPozo;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.Data1;
import thecrimson.co.in.pozo.ProductDataPozo;
import thecrimson.co.in.pozo.ProductPozo;
import thecrimson.co.in.pozo.SubCategoryDataPozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class ProductSubType extends Fragment {
    public MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    RecyclerView recycleview;
    RelativeLayout sort_filter;
    CategoryDataPozo categorydata;
    ArrayList<SubCategoryDataPozo> subcategorydata;
    RecyclerView subcategoryrecycle_view;
    public TextView viewAll;
    private SubCategoryAdapter adapter1;
    int poisitionclicked;
    private ProgressBar progressbar;
    private String tag;

    public ProductSubType() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setHasOptionsMenu(true);
        categorydata = (CategoryDataPozo) getArguments().getSerializable("categorydata");
        subcategorydata = (ArrayList<SubCategoryDataPozo>) getArguments().getSerializable("subcategorydata");
        poisitionclicked = getArguments().getInt("position");
        tag = getArguments().getString("tag");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_product_sub_type, container, false);
        initview(view);
        bindevent();
        if(tag!=null && tag.equals("filter")){

            if(FilterActivity.productdata!=null && FilterActivity.productdata.size()>0) {

                ProductSubTypeAdapter adapter = new ProductSubTypeAdapter(context, categorydata, subcategorydata, poisitionclicked, FilterActivity.productdata);
                recycleview.setAdapter(adapter);
            }else {

                Toast.makeText(context,"No Data Found, Please try again later",Toast.LENGTH_SHORT).show();
            }

        }else {
            if (!NetworkUtil.isNetworkConnected(context)) {
                String message = Constant.NO_INTERNET;
                Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
            } else {
                ApiCallingProduct(poisitionclicked);
            }
        }

        clickListner();
        return view;
    }


    public void ApiCallingProduct(final int poisitionclicked) {

        final Gson gson = new Gson();
        String jsonstring="";
        progressbar.setVisibility(View.VISIBLE);
        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");

        if(poisitionclicked!=-1) {
            Data data = new Data();
            data.setCategory(categorydata.getId());
            data.setSubcategory(subcategorydata.get(poisitionclicked).getId());
            data.setCustomer_id(Integer.parseInt(userid));
            jsonstring = gson.toJson(data);
        }else {
           Data1 data1 = new Data1();
           data1.setCategory(categorydata.getId());
           data1.setCustomer_id(Integer.parseInt(userid));
            jsonstring = gson.toJson(data1);
        }




        AndroidNetworking.post(Constant.Url+"product")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {
                        progressbar.setVisibility(View.GONE);

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            sort_filter.setVisibility(View.VISIBLE);
                            ProductPozo datum = gson.fromJson(response.toString(), ProductPozo.class);

                            List<ProductDataPozo> productdata = datum.getData();

                            ProductSubTypeAdapter adapter = new ProductSubTypeAdapter(context,categorydata,subcategorydata, poisitionclicked,productdata);
                            recycleview.setAdapter(adapter);

                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                            sort_filter.setVisibility(View.GONE);
                            ProductSubTypeAdapter adapter = new ProductSubTypeAdapter(context,categorydata,subcategorydata, poisitionclicked,null);
                            recycleview.setAdapter(adapter);
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void bindevent() {

        txtToolbar.setText(categorydata.getName());

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        context.setSupportActionBar(toolbar);

        context.getSupportActionBar().setDisplayShowTitleEnabled(false);

        adapter1 = new SubCategoryAdapter(categorydata,subcategorydata,context,poisitionclicked, ProductSubType.this);
        subcategoryrecycle_view.setAdapter(adapter1);
        if(poisitionclicked!=-1) {
            subcategoryrecycle_view.smoothScrollToPosition(poisitionclicked);
        }



    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        recycleview=view.findViewById(R.id.recycleview);
        sort_filter=view.findViewById(R.id.sort_filter);
        viewAll=view.findViewById(R.id.viewAll);
        progressbar=view.findViewById(R.id.progressbar);
        subcategoryrecycle_view=view.findViewById(R.id.subcategoryrecycle_view);
        recycleview.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        subcategoryrecycle_view.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
    }
    
    public void changeViallColor(){
        
        viewAll.setTextColor(context.getResources().getColor(R.color.colorlightwhite));
    }

    private void clickListner() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button
                fragment = new ProductTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data",categorydata);
                fragment.setArguments(bundle);
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();
            }
        });

        sort_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(context,FilterActivity.class);
                in.putExtra("position",poisitionclicked);
                in.putExtra("categorydata",categorydata);
                in.putExtra("subcategorydata",subcategorydata);
                startActivity(in);
            }
        });

        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                adapter1.row_index=-2;
                viewAll.setTextColor(context.getResources().getColor(R.color.colorwhite));
                adapter1.notifyDataSetChanged();
                if (!NetworkUtil.isNetworkConnected(context)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                } else {
                    ApiCallingProduct(-1);
                }



            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       this.context= (MainActivity) context;
    }


}
