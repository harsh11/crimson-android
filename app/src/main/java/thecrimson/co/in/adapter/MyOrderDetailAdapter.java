package thecrimson.co.in.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.OrderDetailData;

public class MyOrderDetailAdapter extends RecyclerView.Adapter<MyOrderDetailAdapter.MyViewHolder>{
    List<OrderDetailData> item;
    MainActivity context;
    public MyOrderDetailAdapter(List<OrderDetailData> item, MainActivity context) {
        this.item=item;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_order_detail, parent, false);

        return new MyOrderDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        if(item.get(position).getDiscount().equals("0%")){

            holder.price.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }else {
            holder.price.setVisibility(View.VISIBLE);
            holder.discount.setVisibility(View.VISIBLE);
        }

        holder.name.setText(item.get(position).getProductTittle());
        holder.type.setText(item.get(position).getProduct_category());
        holder.brand.setText(item.get(position).getProduct_brand());
        holder.newprice.setText("Rs " +item.get(position).getSalePrice());
        holder.price.setText("Rs " +item.get(position).getRetailPrice());
        holder.discount.setText("("+item.get(position).getDiscount()+ " " + "off"+")");
        holder.quantity.setText(item.get(position).getQty());

        Picasso.get().load(item.get(position).getImage()).placeholder(R.drawable.placehoderimage).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable ob = new BitmapDrawable(context.getResources(), bitmap);
                holder.image.setBackgroundDrawable(ob);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                holder.image.setBackgroundDrawable(placeHolderDrawable);

            }
        });
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView type;
        TextView brand;
        TextView newprice;
        TextView price;
        TextView discount;
        TextView quantity;
        public MyViewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
            type=itemView.findViewById(R.id.type);
            brand=itemView.findViewById(R.id.brand);
            newprice=itemView.findViewById(R.id.newprice);
            price=itemView.findViewById(R.id.price);
            discount=itemView.findViewById(R.id.discount);
            quantity=itemView.findViewById(R.id.quantity);
        }
    }
}
