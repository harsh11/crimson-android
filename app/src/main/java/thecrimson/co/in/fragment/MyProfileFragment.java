package thecrimson.co.in.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import thecrimson.co.in.activity.MainActivity;
import thecrimson.co.in.R;
import thecrimson.co.in.pozo.Data2;
import thecrimson.co.in.pozo.MyProfileDataPozo;
import thecrimson.co.in.pozo.MyProfilePozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SharedPreference;

public class MyProfileFragment extends Fragment {
    MainActivity context;
    Toolbar toolbar;
    TextView txtToolbar;
    Fragment fragment;
    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmentTransaction;
    TextView full_name;
    TextView company_name;
    TextView email_id;
    TextView phn_number;
    TextView customer_type;
    TextView gstin;
    TextView addressline1;
    TextView addressline2;
    TextView pincode;
    TextView city;
    TextView state;
    ProgressBar progressbar;

    public MyProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_profile, container, false);
        initview(view);
        bindevent();
        clickLisner();
        if (!NetworkUtil.isNetworkConnected(context)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
        } else {
           ApiCalling();
        }
        return view;
    }

    private void ApiCalling() {


        String userid = SharedPreference.getSharedPreferenceString(context,Constant.userid,"");
        Gson gson = new Gson();
        final Data2 data = new Data2();
        data.setId(Integer.parseInt(userid));
        String jsonstring = gson.toJson(data);
        progressbar.setVisibility(View.VISIBLE);

        AndroidNetworking.post(Constant.Url+"showUser")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Gson gson = new Gson();
                            MyProfilePozo datum = gson.fromJson(response.toString(), MyProfilePozo.class);
                            MyProfileDataPozo profiledata = datum.getData();

                            settingdata(profiledata);


                        }else if(code!=null && code.equals("500")){


                            Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(context,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void settingdata(MyProfileDataPozo profiledata) {

        if(profiledata.getFullname()!=null && !profiledata.getFullname().equals("")) {
            full_name.setText(profiledata.getFullname());
        }else {
            full_name.setText("-");
        }
        if(profiledata.getCompanyName()!=null && !profiledata.getCompanyName().equals("")) {
            company_name.setText(profiledata.getCompanyName());
        }else {
            company_name.setText("-");
        }
        if(profiledata.getEmail()!=null && !profiledata.getEmail().equals("")) {
            email_id.setText(profiledata.getEmail());
        }else {

            email_id.setText("-");
        }
        if(profiledata.getContactNo()!=null && !profiledata.getContactNo().equals("")) {
            phn_number.setText(profiledata.getContactNo());
        }else {
            phn_number.setText("-");
        }
        if(profiledata.getCustomerType()!=null && !profiledata.getCustomerType().equals("")) {
            customer_type.setText(profiledata.getCustomerType());
        }else {
            customer_type.setText("-");
        }
        if(profiledata.getGsitn()!=null && !profiledata.getGsitn().equals("")) {
            gstin.setText(profiledata.getGsitn());
        }else {
            gstin.setText("-");
        }
        if(profiledata.getAddress()!=null && !profiledata.getAddress().equals("")) {
            addressline1.setText(profiledata.getAddress());
        }else {
            addressline1.setText("-");
        }

        if(profiledata.getAddressTwo()!=null && !profiledata.getAddressTwo().equals("")){
            addressline2.setText(profiledata.getAddressTwo());
        }else {

            addressline2.setText("-");
        }

        if(profiledata.getPostcode()!=null && !profiledata.getPostcode().equals("")){

            pincode.setText(profiledata.getPostcode());
        }else {
            pincode.setText("-");
        }

        if(profiledata.getRegion()!=null && !profiledata.getRegion().equals("")){

            state.setText(profiledata.getRegion());
        }else {

            state.setText("-");
        }

        if(profiledata.getCity()!=null && !profiledata.getCity().equals("")){
            city.setText(profiledata.getCity());
        }else {
            city.setText("-");
        }
    }

    private void clickLisner() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open navigation drawer when click navigation back button

                fragment = new HomeFragment();
                fragmentmanager = context.getSupportFragmentManager();
                fragmentTransaction = fragmentmanager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, fragment);
                fragmentTransaction.commit();
                fragmentmanager.popBackStack();


            }
        });
    }

    private void bindevent() {

        txtToolbar.setText("MY PROFILE");

        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));
    }

    private void initview(View view) {

        toolbar = view.findViewById(R.id.toolbar);
        txtToolbar=view.findViewById(R.id.toolbar_title);
        full_name=view.findViewById(R.id.full_name);
        company_name=view.findViewById(R.id.company_name);
        email_id=view.findViewById(R.id.email_id);
        phn_number=view.findViewById(R.id.phn_number);
        customer_type=view.findViewById(R.id.customer_type);
        gstin=view.findViewById(R.id.gstin);
        addressline1=view.findViewById(R.id.addressline1);
        addressline2=view.findViewById(R.id.addressline2);
        pincode=view.findViewById(R.id.pincode);
        city=view.findViewById(R.id.city);
        state=view.findViewById(R.id.state);
        progressbar=view.findViewById(R.id.progressbar);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;

    }

}
