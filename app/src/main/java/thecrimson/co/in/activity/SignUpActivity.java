package thecrimson.co.in.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import thecrimson.co.in.R;
import thecrimson.co.in.pozo.CityDataPozo;
import thecrimson.co.in.pozo.CityPozo;
import thecrimson.co.in.pozo.CustomerTypeDataPozo;
import thecrimson.co.in.pozo.CustomerTypePozo;
import thecrimson.co.in.pozo.Data;
import thecrimson.co.in.pozo.StateDataPozo;
import thecrimson.co.in.pozo.StatePozo;
import thecrimson.co.in.util.Constant;
import thecrimson.co.in.util.NetworkUtil;
import thecrimson.co.in.util.SearchableListDialog;

public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener,View.OnClickListener {

    EditText ed_customertype;
    @NotEmpty
    EditText ed_companyname;
    @Email
    EditText ed_email;
    @Length(max = 10,min = 10,message = "Phone number must be of 10 digit")
    EditText ed_phno;
    @Password
    EditText ed_pasword;
    @NotEmpty
    @ConfirmPassword
    EditText ed_confrmpasword;
    @NotEmpty
    EditText ed_fullname;
    EditText ed_gsitn;
    EditText ed_state;
    EditText ed_city;
    EditText ed_addressline1;
    EditText ed_addressline2;
    EditText ed_pincode;
    TextView tv_signup;
    TextView tv_login;
    private Validator validator;
    private SearchableListDialog _searchableListDialog;
    private SearchableListDialog _searchablecityListDialog;
    private SearchableListDialog _searchablestateListDialog;
    List<String> statelist;
    List<String> citylist;
    List<String> customertypelist;
    private int customertypeid;
    private int cityid;
    private int stateid;
    ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initview();
        clikLIstener();
        if (!NetworkUtil.isNetworkConnected(this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        } else {
            customertypeApiCalling();
            stateApiCalling();
        }

    }

    private void customertypeApiCalling() {

        customertypelist=new ArrayList<>();

        AndroidNetworking.get(Constant.Url+"customer-type")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){
                            Gson gson = new Gson();
                            CustomerTypePozo data = gson.fromJson(response.toString(), CustomerTypePozo.class);

                            List<CustomerTypeDataPozo> datum = data.getData();

                            for (int i=0;i<datum.size();i++){

                                customertypelist.add(datum.get(i).getType());
                            }

                            setcustomerType(customertypelist,datum);
                        }else if(code!=null && code.equals("500")){

                            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        Toast.makeText(SignUpActivity.this,anError.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void stateApiCalling() {

       statelist = new ArrayList<>();

        AndroidNetworking.get(Constant.Url+"state")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){
                            Gson gson = new Gson();
                            StatePozo data = gson.fromJson(response.toString(), StatePozo.class);

                            List<StateDataPozo> datum = data.getData();

                            for (int i=0;i<datum.size();i++){

                                statelist.add(datum.get(i).getState());
                            }

                            setState(statelist,datum);
                        }else if(code!=null && code.equals("500")){

                            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        Toast.makeText(SignUpActivity.this,anError.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setCity(final List<String> citylist, final List<CityDataPozo> datum) {

        _searchablecityListDialog = SearchableListDialog.newInstance(citylist);
        _searchablecityListDialog.setTitle("Select City");

        _searchablecityListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {


            @Override
            public void onSearchableItemClicked(Object item, int position) {

                ed_city.setError(null);

                ed_city.setText(item.toString());

                for(int i=0;i<datum.size();i++){

                    if(item.toString().equals(datum.get(i).getCityname())){

                        cityid = datum.get(i).getId();
                        break;
                    }
                }

            }
        });
    }

    private void setState(final List<String> statelist, final List<StateDataPozo> datum) {


        _searchablestateListDialog = SearchableListDialog.newInstance(statelist);
        _searchablestateListDialog.setTitle("Select State");

        _searchablestateListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {

            @Override
            public void onSearchableItemClicked(Object item, int position) {

                ed_state.setError(null);

                ed_state.setText(item.toString());
                ed_city.setText("");

                for(int i=0;i<datum.size();i++){

                    if(item.toString().equals(datum.get(i).getState())){

                        stateid = datum.get(i).getId();
                        break;
                    }
                }

                if (!NetworkUtil.isNetworkConnected(SignUpActivity.this)) {
                    String message = Constant.NO_INTERNET;
                    Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
                } else {
                    cityApiCalling(stateid);
                }

            }
        });
    }

    private void cityApiCalling(int stateid) {

        citylist = new ArrayList<>();

        AndroidNetworking.post(Constant.Url+"city")
                .setContentType("application/json; charset=utf-8")
                .addBodyParameter("state", String.valueOf(stateid))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){
                            Gson gson = new Gson();
                            CityPozo data = gson.fromJson(response.toString(), CityPozo.class);

                            List<CityDataPozo> datum = data.getData();

                            for (int i=0;i<datum.size();i++){

                                citylist.add(datum.get(i).getCityname());
                            }

                            setCity(citylist,datum);
                        }else if(code!=null && code.equals("500")){
                            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(SignUpActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void setcustomerType(List<String> customertypelist, final List<CustomerTypeDataPozo> datum) {

        _searchableListDialog = SearchableListDialog.newInstance(customertypelist);
        _searchableListDialog.setTitle("Select Customer Type");

        _searchableListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {

            @Override
            public void onSearchableItemClicked(Object item, int position) {

                ed_customertype.setError(null);

                ed_customertype.setText(item.toString());

                for(int i=0;i<datum.size();i++){

                    if(item.toString().equals(datum.get(i).getType())){

                        customertypeid = datum.get(i).getId();
                        break;
                    }
                }
            }
        });
    }

    private void clikLIstener() {
        validator.setValidationListener(this);
        tv_signup.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        ed_customertype.setOnClickListener(this);
        ed_city.setOnClickListener(this);
        ed_state.setOnClickListener(this);

    }

    private void initview() {
        validator = new Validator(this);

        ed_fullname = findViewById(R.id.ed_fullname);
        ed_customertype = findViewById(R.id.ed_customertype);
        ed_companyname = findViewById(R.id.ed_companyname);
        ed_addressline2 = findViewById(R.id.ed_addressline2);
        ed_gsitn = findViewById(R.id.ed_gsitn);
        ed_state = findViewById(R.id.ed_state);
        ed_email = findViewById(R.id.ed_email);
        ed_pincode = findViewById(R.id.ed_pincode);
        ed_addressline1 = findViewById(R.id.ed_addressline1);
        ed_phno = findViewById(R.id.ed_phno);
        ed_city = findViewById(R.id.ed_city);
        ed_pasword = findViewById(R.id.ed_pasword);
        ed_confrmpasword = findViewById(R.id.ed_confrmpasword);
        tv_signup = findViewById(R.id.tv_signup);
        tv_login = findViewById(R.id.tv_login);
        progressbar = findViewById(R.id.progressbar);


        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/LatoSemibold.ttf");
        tv_login.setTypeface(custom_font);
    }

    @Override
    public void onValidationSucceeded() {

        if (!NetworkUtil.isNetworkConnected(SignUpActivity.this)) {
            String message = Constant.NO_INTERNET;
            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
        } else {
            ApiCalling();
        }



    }

    private void ApiCalling() {

        progressbar.setVisibility(View.VISIBLE);

        Data data =  new Data();
        data.setFullname(ed_fullname.getText().toString());
        data.setCompany_name(ed_companyname.getText().toString());
        data.setCustomer_type(customertypeid);
        data.setEmail(ed_email.getText().toString());
        data.setContact_no(ed_phno.getText().toString());
        data.setState(stateid);
        data.setCity(cityid);
        data.setGsitn(ed_gsitn.getText().toString());
        data.setAddress_one(ed_addressline1.getText().toString());
        data.setAddress_two(ed_addressline2.getText().toString());
        data.setPincode(ed_pincode.getText().toString());
        data.setPassword(ed_pasword.getText().toString());
        data.setPassword_confirmation(ed_confrmpasword.getText().toString());

        Gson gson = new Gson();

        String jsonstring = gson.toJson(data);

        Log.e("jsonstring",jsonstring);

        AndroidNetworking.post(Constant.Url+"signup")
                .setContentType("application/json; charset=utf-8")
                .addStringBody(jsonstring)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    public String code;
                    public String message;

                    @Override
                    public void onResponse(JSONObject response) {

                        progressbar.setVisibility(View.GONE);
                        try {
                            message =  response.getString("msg");
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(code!=null && code.equals("200")){

                            Toast.makeText(SignUpActivity.this,"Registration done successfully",Toast.LENGTH_SHORT).show();
//                            Intent in = new Intent(SignUpActivity.this,MainActivity.class);
//                            startActivity(in);
                            finish();

                        }else if(code!=null && code.equals("500")){

                            Toast.makeText(SignUpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        progressbar.setVisibility(View.GONE);
                        // handle error
                        Toast.makeText(SignUpActivity.this,error.getErrorDetail(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_signup:

                validator.validate();
                break;

            case R.id.tv_login:
                Intent in = new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(in);
                finish();
                break;

            case R.id.ed_customertype:
                if(_searchableListDialog!=null){

                    _searchableListDialog.show(this.getSupportFragmentManager(), "");
                }
                break;

            case R.id.ed_city:
                if(_searchablecityListDialog!=null){

                    _searchablecityListDialog.show(this.getSupportFragmentManager(), "");
                }
                break;

            case R.id.ed_state:
                if(_searchablestateListDialog!=null){

                    _searchablestateListDialog.show(this.getSupportFragmentManager(), "");
                }
                break;
        }
    }
}
