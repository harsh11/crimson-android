package thecrimson.co.in.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class NetworkUtil {

    public static boolean isNetworkConnected(Context mContext) {
//        if (mContext == null) mContext = ReferenceApplication.getAppContext();
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            View currentFocus = activity.getCurrentFocus();
            if (null != currentFocus) {
                IBinder windowToken = currentFocus.getWindowToken();
                if (null != windowToken) {
                    inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
                }
            }
        }
    }
}
